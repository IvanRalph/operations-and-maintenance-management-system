<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee_details';
    public $primaryKey = 'id';

    public $timestamps = false;

    public $fillable = ['employee_id', 'birth_date','firstname', 'name_extension', 'middlename', 'lastname', 'birth_place', 'gender', 'civil_status', 'religion', 'citizenship', 'date_hired', 'department_id', 'position', 'level', 'employee_status', 'regularization_date', 'separation_date', 'site', 'tax_status', 'sss', 'tin', 'hdmf', 'phic', 'bpi', 'salary'];

    public function account()
    {
        return $this->hasOne('App\User', 'employee_id');
    }

    protected $connection = 'mysql3';
}
