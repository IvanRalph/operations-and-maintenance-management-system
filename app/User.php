<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserAccess;
use App\Employee;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    public $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'employee_id', 'status', 'isDeleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $connection = 'mysql2';

    public function isUserType(){
        $userType = UserAccess::leftJoin('tjsg_hris.employee_details', 'user_access_details.employee_id', 'employee_details.id')
        ->join('user_types', 'user_access_details.user_type_id', 'user_types.id')
        ->where('employee_details.employee_id', $this->employee_id)
        ->select('user_types.user_type')
        ->first();

        return $userType ? $userType->user_type : '';
    }

    public function getDetails(){
        return Employee::where('employee_id', $this->employee_id)->first();
    }
}
