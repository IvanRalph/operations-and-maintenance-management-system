<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    protected $table = 'incident_details';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['ir_no', 'project_id', 'capacity', 'prepared_by', 'status', 'status_remarks', 'status_date', 'brief_description', 'incident_details', 'other_comments'];
}
