<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldActivityUpload extends Model
{
    protected $table = 'field_activity_upload_details';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['far_no', 'filename', 'deleted'];
}
