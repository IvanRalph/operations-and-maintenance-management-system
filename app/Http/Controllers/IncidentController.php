<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Incident;
use App\IncidentUpload;
use Storage;
use App\IncidentStatus;
use App\Project;
use App\AuditTrail;

class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::denies('access-matrix', 4)){
            return redirect('/');
        }

        return view('pages.incidents');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(\Gate::denies('access-matrix', 3)){
            return redirect('/');
        }
        if(old('project')){
            $project = Project::where('id', old('project'))->first();
            return view('pages.createIncident')->with('projectOld', $project);
        }
        return view('pages.createIncident');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'project'=>'required|exists:project_details,id',
            'location'=>'exists:project_details,location',
            'plant_id'=>'exists:project_details,plant_id',
            'dc_capacity'=>'exists:project_details,dc_capacity',
            'brief_description'=>'required|max:300',
            'incident_details'=>'required|max:300',
            'other_comments'=>'nullable|max:300',
            'attachments.*' => 'nullable|file|mimes:xlsx,xls,csv,jpg,jpeg,png,bmp,doc,docx,pdf,tif,tiff'
        ],[
            'attachments.*.file'=>'The attachment must be a file.',
            'attachments.*.mimes'=>'The attachments must be a file of type: xlsx, xls, csv, jpg, jpeg, png, bmp, doc, docx, pdf, tif, tiff.'
        ]);
        if($validator->fails()){
            $request->flash();
            AuditTrail::create([
                'module'=>'Incidents',
                'submodule'=>'Create incident',
                'action'=>'Validation error on create',
                'created_by'=>\Auth::user()->id
            ]);
            return redirect('incidents/create')
                    ->withErrors($validator);
        }

        $incident = Incident::orderBy('id', 'DESC')->first();

        if(!$incident){
            $incident = new \stdClass();
            $incident->id = 1;
        }else{
            $incident->id += 1;
        }

        $pre = '';
        for($i = 4; $i > strlen($incident->id); $i--){
            $pre .= '0';
        }

        $files = $request->file('attachments');

        $inc = Incident::create([
            'ir_no'=>'IR-' . $pre . $incident->id,
            'project_id'=>$request->input('project'),
            'prepared_by'=>Auth::user()->id,
            'brief_description'=>$request->input('brief_description'),
            'incident_details'=>$request->input('incident_details'),
            'other_comments'=>$request->input('other_comments') != null ? $request->input('other_comments') : '',
            'status'=>'Open',
            'status_date'=>Date('Y-m-d')
        ]);

        if(!empty($files) && \Gate::allows('access-matrix', 7)){
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $ext = $file->extension();
                $fnameNoExt = pathinfo($filename,PATHINFO_FILENAME);
                $path = storage_path("app\\public\\o&m\\" . $filename);
                if (\File::exists($path)) {
                    $i = 1;
                    while(\File::exists(storage_path("app\\public\\o&m\\" . $fnameNoExt . '('. $i .').' . $ext))) $i++;
                    $filename = $fnameNoExt . '(' . $i . ').' . $ext;
                }
                Storage::disk('local')->put('public/o&m/'.$filename, file_get_contents($file->getRealPath()));

                IncidentUpload::create([
                    'ir_no'=>$incident->id,
                    'filename'=>$filename
                ]);
            }
        }

        AuditTrail::create([
            'module'=>'Incidents',
            'submodule'=>'Create incident',
            'action'=>'Created ' . $inc->ir_no,
            'created_by'=>\Auth::user()->id
        ]);

        return redirect('/incidents')->with('incidentCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(\Gate::denies('access-matrix', 4)){
            return redirect('/');
        }

        if(isset($_GET['type'])){
            $incident = Incident::join('project_details', 'incident_details.project_id', 'project_details.id')->where('incident_details.ir_no', $id)->select('incident_details.id' ,'incident_details.ir_no', 'project_details.project_name AS project_name', 'project_details.location AS location', 'project_details.plant_id AS plant_id', 'project_details.dc_capacity AS dc_capacity', 'incident_details.brief_description', 'incident_details.incident_details', 'incident_details.other_comments', 'incident_details.status', 'incident_details.status_remarks', 'incident_details.id')->firstOrFail();
            $file = IncidentUpload::where('ir_no', $incident->id)->get();
            $incident->file = $file;
            $status = IncidentStatus::join('tjsg_hris.employee_details', 'incident_status_tracker.updated_by', 'employee_details.id')
            ->select('incident_status_tracker.status', 'incident_status_tracker.remarks', 'incident_status_tracker.created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = incident_status_tracker.updated_by) AS updated_by'))
            ->where('incident_status_tracker.ir_no', $incident->id)->get();
            $incident->tracker = $status;
            return $incident;
        }

        $incident = Incident::join('project_details', 'incident_details.project_id', 'project_details.id')
        ->join('tjsg_hris.employee_details', 'incident_details.prepared_by', 'tjsg_hris.employee_details.id')
        ->select('project_details.project_name', 'incident_details.ir_no', 'project_details.location', 'project_details.plant_id', 'project_details.dc_capacity', 'incident_details.brief_description', 'incident_details.incident_details', 'incident_details.other_comments', 'incident_details.status', 'incident_details.status_remarks', 'incident_details.id')
        ->where('incident_details.id', $id)
        ->firstOrFail();

        $file = IncidentUpload::where(['ir_no'=>$incident->id, 'deleted'=>0])->get();

        $status = IncidentStatus::join('tjsg_hris.employee_details', 'incident_status_tracker.updated_by', 'employee_details.id')
        ->select('incident_status_tracker.status', 'incident_status_tracker.remarks', 'incident_status_tracker.created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = incident_status_tracker.updated_by) AS updated_by'))
        ->where('incident_status_tracker.ir_no', $incident->id)->get();

        AuditTrail::create([
            'module'=>'Incidents',
            'submodule'=>'View Incident',
            'action'=>'Viewed ' . $incident->ir_no,
            'created_by'=>\Auth::user()->id
        ]);

        return view('pages.createIncident')->with('showIncident', $incident)->with('file', $file)->with('status', $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(\Gate::denies('access-matrix', 5)){
            return redirect('/');
        }

        $incident = Incident::join('project_details', 'incident_details.project_id', 'project_details.id')
        ->join('tjsg_hris.employee_details', 'incident_details.prepared_by', 'tjsg_hris.employee_details.id')
        ->select('project_details.project_name', 'incident_details.ir_no', 'project_details.location', 'project_details.plant_id', 'project_details.dc_capacity', 'incident_details.brief_description', 'incident_details.incident_details', 'incident_details.other_comments', 'incident_details.status', 'incident_details.status_remarks', 'project_details.id AS project_id', 'incident_details.id AS incident_id')
        ->where('incident_details.id', $id)
        ->firstOrFail();

        $file = IncidentUpload::where(['ir_no'=>$incident->incident_id, 'deleted'=>0])->get();

        return view('pages.createIncident')->with('updateIncident', $incident)->with('file', $file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(\Gate::denies('access-matrix', 5)){
            return redirect('/');
        }
        if(Auth::user()->isUserType() == 'Operation'){
            $this->validate($request, [
                'status'=>'required|in:Open,Reopen,Closed,Unresolved',
                'status_remarks'=>'required|max:300'
            ]);

            $incident = Incident::where('id', $id)->first();

            Incident::where('id', $id)->update([
                'status'=>$request->input('status'),
                'status_remarks'=>$request->input('status_remarks')
            ]);

            $status = new IncidentStatus;
            $status->ir_no = $id;
            $status->status = $request->input('status');
            $status->remarks = $request->input('status_remarks');
            $status->updated_by = Auth::user()->id;
            $status->save();

            $files = $request->file('attachments');

            if(!empty($files) && \Gate::allows('access-matrix', 7)){
                foreach($files as $file){
                    $filename = $file->getClientOriginalName();
                    $ext = $file->extension();
                    $fnameNoExt = pathinfo($filename,PATHINFO_FILENAME);
                    $path = storage_path("app\\public\\o&m\\" . $filename);
                    if (\File::exists($path)) {
                        $i = 1;
                        while(\File::exists(storage_path("app\\public\\o&m\\" . $fnameNoExt . '('. $i .').' . $ext))) $i++;
                        $filename = $fnameNoExt . '(' . $i . ').' . $ext;
                    }
                    Storage::disk('local')->put('public/o&m/'.$filename, file_get_contents($file->getRealPath()));

                    IncidentUpload::create([
                        'ir_no'=>$id,
                        'filename'=>$filename
                    ]);
                }
            }

            AuditTrail::create([
                'module'=>'Incidents',
                'submodule'=>'Create incident',
                'action'=>'Updated ' . $incident->ir_no,
                'created_by'=>\Auth::user()->id
            ]);

            return view('pages.incidents')->with('incidentUpdated', true);
        }else{
            $this->validate($request, [
                'project'=>'required|exists:project_details,id',
                'location'=>'exists:project_details,location',
                'plant_id'=>'exists:project_details,plant_id',
                'capacity'=>'exists:project_details,dc_capacity',
                'brief_description'=>'nullable|max:300',
                'incident_details'=>'required|max:300',
                'other_comments'=>'nullable|max:300'
            ]);

            $incident = Incident::where('id', $id)->first();

            Incident::where('id', $id)->update([
                'project_id'=>$request->input('project'),
                'brief_description'=>$request->input('brief_description'),
                'incident_details'=>$request->input('incident_details'),
                'other_comments'=>$request->input('other_comments')
            ]);

            $files = $request->file('attachments');

            if(!empty($files)){
                foreach($files as $file){
                    $filename = $file->getClientOriginalName();
                    $ext = $file->extension();
                    $fnameNoExt = pathinfo($filename,PATHINFO_FILENAME);
                    $path = storage_path("app\\public\\o&m\\" . $filename);
                    if (\File::exists($path)) {
                        $i = 1;
                        while(\File::exists(storage_path("app\\public\\o&m\\" . $fnameNoExt . '('. $i .').' . $ext))) $i++;
                        $filename = $fnameNoExt . '(' . $i . ').' . $ext;
                    }
                    Storage::disk('local')->put('public/o&m/'.$filename, file_get_contents($file->getRealPath()));

                    IncidentUpload::create([
                        'ir_no'=>$id,
                        'filename'=>$filename
                    ]);
                }
            }

            AuditTrail::create([
                'module'=>'Incidents',
                'submodule'=>'Create incident',
                'action'=>'Updated ' . $incident->ir_no,
                'created_by'=>\Auth::user()->id
            ]);

            return redirect('/incidents')->with('incidentUpdated', true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->input('type') && $request->input('type') == 'deleteAttachment'){
            if(\Gate::allows('delete-incident-attachment', $id)){
                $incident = IncidentUpload::where('id', $id)->update([
                    'deleted'=>1
                ]);

                if($incident){
                    AuditTrail::create([
                        'module'=>'Incidents',
                        'submodule'=>'Delete incident',
                        'action'=>'Deleted Attachment ID: ' . $id,
                        'created_by'=>\Auth::user()->id
                    ]);

                    return ['title'=>'Deleted', 'msg'=>'Attachment deleted successfully', 'status'=>'success'];
                }else{
                    \Log::alert('Error in deleting attachment', ['Attachment'=>$incident]);
                    return ['title'=>'Error', 'msg'=>'There was an error in deleting the attachment', 'status'=>'error'];
                }
            }else{
                \Log::alert('Error in deleting attachment. Gate denied request. [delete-incident-attachment]');
                return ['title'=>'Error', 'msg'=>'There was an error in deleting the attachment', 'status'=>'error'];
            }
        }

        if(\Gate::denies('delete-incident', $id) || \Gate::denies('access-matrix', 6)){
            \Log::alert('Gate denied deletion of incident report.', ['user id: '=>\Auth::user()->id, 'ir_no'=>$id]);
            return ['title'=>'Error', 'msg'=>'An error occured when trying to delete your request.', 'type'=>'error'];
        }

        $checkJo = Incident::join('job_order_details', 'incident_details.id', 'job_order_details.ir_no')
        ->select('job_order_details.jo_no as jo_no')->first();

        if($checkJo){
            return ['title'=>'Error', 'msg'=>'The Incident Report you are trying to delete has an existing Job Order.', 'type'=>'error'];
        }

        $incident = Incident::where('id', $id)->update([
            'status'=>'Deleted'
        ]);

        if($incident){
            $incident = Incident::where('id', $id)->first();
            AuditTrail::create([
                'module'=>'Incidents',
                'submodule'=>'Create incident',
                'action'=>'Deleted ' . $incident->ir_no,
                'created_by'=>\Auth::user()->id
            ]);
            return ['title'=>'Deleted', 'msg'=>'Incident Report deleted successfully.', 'type'=>'success'];
        }else{
            \Log::alert('Error on deleting incident report.', ['Incident'=>$incident]);
            return ['title'=>'Error', 'msg'=>'An error occured when trying to delete your request.', 'type'=>'error'];
        }
    }

    public function previewPdf($id){
        if(\Gate::denies('access-matrix', 8)){
            return redirect('/');
        }
        $incident = Incident::join('project_details', 'incident_details.project_id', 'project_details.id')->where('incident_details.id', $id)
        ->join('tjsg_hris.employee_details', 'incident_details.prepared_by', 'employee_details.id')
        ->select('incident_details.id' ,'incident_details.ir_no', 'project_details.project_name AS project_name', 'project_details.location AS location', 'project_details.plant_id AS plant_id', 'project_details.dc_capacity AS dc_capacity', 'incident_details.brief_description', 'incident_details.incident_details', 'incident_details.other_comments', 'incident_details.status', 'incident_details.status_remarks', 'incident_details.id', 'incident_details.created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = incident_details.prepared_by) AS prepared_by'))->firstOrFail();
        $file = IncidentUpload::where('ir_no', $incident->id)->get();
        $incident->file = $file;
        $status = IncidentStatus::join('tjsg_hris.employee_details', 'incident_status_tracker.updated_by', 'employee_details.id')
        ->select('incident_status_tracker.status', 'incident_status_tracker.remarks', 'incident_status_tracker.created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = incident_status_tracker.updated_by) AS updated_by'))
        ->where('incident_status_tracker.ir_no', $incident->id)->get();
        $incident->tracker = $status;
        $pdf = \PDF::loadView('pdf.incidentreport', compact('incident'))->setPaper('a4', 'portrait');
        AuditTrail::create([
            'module'=>'Incidents',
            'submodule'=>'Preview PDF',
            'action'=>'Previewed PDF for ' . $incident->ir_no,
            'created_by'=>\Auth::user()->id
        ]);
        return $pdf->stream();
    }

    public function downloadPdf($id){
        if(\Gate::denies('access-matrix', 8)){
            return redirect('/');
        }
        $incident = Incident::join('project_details', 'incident_details.project_id', 'project_details.id')->where('incident_details.id', $id)->select('incident_details.id' ,'incident_details.ir_no', 'project_details.project_name AS project_name', 'project_details.location AS location', 'project_details.plant_id AS plant_id', 'project_details.dc_capacity AS dc_capacity', 'incident_details.brief_description', 'incident_details.incident_details', 'incident_details.other_comments', 'incident_details.status', 'incident_details.status_remarks', 'incident_details.id', 'incident_details.created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = incident_details.prepared_by) AS prepared_by'))->firstOrFail();
        $file = IncidentUpload::where('ir_no', $incident->id)->get();
        $incident->file = $file;
        $status = IncidentStatus::join('tjsg_hris.employee_details', 'incident_status_tracker.updated_by', 'employee_details.id')
        ->select('incident_status_tracker.status', 'incident_status_tracker.remarks', 'incident_status_tracker.created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = incident_status_tracker.updated_by) AS updated_by'))
        ->where('incident_status_tracker.ir_no', $incident->id)->get();
        $incident->tracker = $status;
        $pdf = \PDF::loadView('pdf.incidentreport', compact('incident'))->setPaper('a4', 'portrait');
        AuditTrail::create([
            'module'=>'Incidents',
            'submodule'=>'Download PDF',
            'action'=>'Downloaded PDF for ' . $incident->ir_no,
            'created_by'=>\Auth::user()->id
        ]);
        return $pdf->download($incident->ir_no .'.pdf');
    }
}
