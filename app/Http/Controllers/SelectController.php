<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Incident;
use App\JobOrder;
use Illuminate\Support\Facades\Log;

class SelectController extends Controller
{
    public function project(Request $request){
    	$term = trim($request->term);

    	if (empty($term)) {
    	    $data = Project::where('isDeleted', 0)->get();
    	}else{
    	    $data = Project::select('project_name', 'id')
    	    ->where('project_name', 'LIKE', '%'. $term .'%')
            ->where('isDeleted', 0)->get();
    	}

    	$formatted_tags = [];

    	foreach ($data as $tag) {
    	    $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->project_name];
    	}

    	return response()->json($formatted_tags);
    }

    public function incident(Request $request){
        $term = trim($request->term);

        if (empty($term)) {
            $data = Incident::whereIn('status', ['Open', 'Reopen'])->get();
        }else{
            $data = Incident::whereIn('status', ['Open', 'Reopen'])->select('ir_no')
            ->where('ir_no', 'LIKE', '%'. $term .'%')->get();
        }

        $formatted_tags = [];

        foreach ($data as $tag) {
            $formatted_tags[] = ['id' => $tag->ir_no, 'text' => $tag->ir_no];
        }

        return response()->json($formatted_tags);
    }

    public function jobOrder(Request $request){ 
        $term = trim($request->term);

        if (empty($term)) {
            $data = JobOrder::leftJoin('field_activity_details', 'job_order_details.id', 'field_activity_details.jo_no')
            ->select('job_order_details.jo_no AS jo_no')->groupBy('job_order_details.jo_no')
            ->havingRaw('COUNT(IF(field_activity_details.isDeleted = 0, 1, NULL)) < 1')->get();
        }else{
            $data = JobOrder::leftJoin('field_activity_details', 'job_order_details.id', 'field_activity_details.jo_no')
            ->select('job_order_details.jo_no AS jo_no')->groupBy('job_order_details.jo_no')
            ->where('job_order_details.jo_no', 'LIKE', '%'. $term .'%')
            ->havingRaw('COUNT(IF(field_activity_details.isDeleted = 0, 1, NULL)) < 1')->get();
        }

        $formatted_tags = [];

        foreach ($data as $tag) {
            $formatted_tags[] = ['id' => $tag->jo_no, 'text' => $tag->jo_no];
        }

        return response()->json($formatted_tags);
    }
}
