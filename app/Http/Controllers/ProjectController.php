<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Project;
use App\User;
use App\AuditTrail;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.setupProject');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.setupProjectCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'project_name'=>'required',
            'plant_type'=>'required|in:PPA,EPC,FIT',
            'dc_capacity'=>'required',
            'ac_capacity'=>'required',
            'location'=>'required',
            'date_commissioned'=>'required|date',
            'pv_module'=>'required',
            'inverter'=>'required',
            'communication'=>'required'
        ]);

        $proj = Project::orderBy('id', 'DESC')->first();

        if(!$proj){
            $proj = new \stdClass();
            $proj->id = 1;
        }else{
            $proj->id += 1;
        }

        $pre = '';
        for($i = 4; $i > strlen($proj->id); $i--){
            $pre .= '0';
        }

        Project::create([
            'project_name'=>$request->input('project_name'),
            'plant_id'=>'PID-'.$pre.$proj->id,
            'plant_type'=>$request->input('plant_type'),
            'dc_capacity'=>$request->input('dc_capacity'),
            'ac_capacity'=>$request->input('ac_capacity'),
            'location'=>$request->input('location'),
            'date_commissioned'=>$request->input('date_commissioned'),
            'pv_modules'=>$request->input('pv_module'),
            'inverter'=>$request->input('inverter'),
            'communication'=>$request->input('communication'),
            'created_by'=>Auth::user()->id
        ]);

        AuditTrail::create([
            'module'=>'Projects',
            'submodule'=>'Create project',
            'action'=>'Created ' . $request->input('project_name'),
            'created_by'=>\Auth::user()->id
        ]);

        return view('pages.setupProject')->with('projectCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::where('id', $id)->firstOrFail();
        if(isset($_GET['type'])){
            return $project;
        }
        if(\Auth::user()->isUserType() == 'Operation'){
            return redirect('/');
        }

        AuditTrail::create([
            'module'=>'Projects',
            'submodule'=>'View project',
            'action'=>'Viewed ' . $project->project_name,
            'created_by'=>\Auth::user()->id
        ]);

        return view('pages.setupProjectCreate')->with('showProject', $project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::where('id', $id)->firstOrFail();

        return view('pages.setupProjectCreate')->with('updateProject', $project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'project_name'=>'required',
            'plant_type'=>'required|in:PPA,EPC,FIT',
            'dc_capacity'=>'required',
            'ac_capacity'=>'required',
            'location'=>'required',
            'date_commissioned'=>'required|date',
            'pv_module'=>'required',
            'inverter'=>'required',
            'communication'=>'required'
        ]);

        Project::where('id', $id)->update([
            'project_name'=>$request->input('project_name'),
            'plant_type'=>$request->input('plant_type'),
            'dc_capacity'=>$request->input('dc_capacity'),
            'ac_capacity'=>$request->input('ac_capacity'),
            'location'=>$request->input('location'),
            'date_commissioned'=>$request->input('date_commissioned'),
            'pv_modules'=>$request->input('pv_module'),
            'inverter'=>$request->input('inverter'),
            'communication'=>$request->input('communication'),
            'updated_by'=>\Auth::user()->id
        ]);

        AuditTrail::create([
            'module'=>'Projects',
            'submodule'=>'Update project',
            'action'=>'Updated ' . $request->input('project_name'),
            'created_by'=>\Auth::user()->id
        ]);

        return view('pages.setupProject')->with('projectUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proj = Project::find($id);
        if(\Gate::denies('delete-project', $proj)){
            \Log::alert('Gate denied deletion of Project. Reason: User is not the creator.', ['user id: '=>\Auth::user()->id, 'Project'=>$proj->id]);
            return ['title'=>'Error', 'msg'=>'An error occured when trying to delete your request.', 'type'=>'error'];
        }

        Project::where('id', $id)->update([
            'isDeleted'=>1
        ]);

        AuditTrail::create([
            'module'=>'Projects',
            'submodule'=>'Delete project',
            'action'=>'Deleted ' . $proj->project_name,
            'created_by'=>\Auth::user()->id
        ]);

        return ['title'=>'Deleted', 'msg'=>'Project deleted successfully.', 'type'=>'success'];
    }
}
