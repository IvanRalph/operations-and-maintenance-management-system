<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    public function storage($filename){
        $path = storage_path("app\\public\\o&m\\" . $filename);
        if (!\File::exists($path)) {
            abort(404);
        }
        
        $file = \File::get($path);
        $type = \File::mimeType($path);

        $response = \Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
}
