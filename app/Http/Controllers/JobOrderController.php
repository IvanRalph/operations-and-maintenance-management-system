<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\JobOrder;
use App\Incident;
use App\JobOrderUpload;
use App\AuditTrail;
use Storage;

class JobOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::denies('access-matrix', 11)){
            return redirect('/');
        }
        return view('pages.jobOrders');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(\Gate::denies('access-matrix', 10)){
            return redirect('/');
        }

        return view('pages.createJobOrder');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'ir_no'=>'required|exists:incident_details,ir_no',
            'asset_description'=>'required|max:300',
            'priority_level'=>'required|in:Normal,High',
            'schedule'=>'nullable|date',
            'brief_description'=>'required|max:300',
            'details'=>'required|max:300',
            'remarks'=>'required|max:300',
            'attachments.*' => 'nullable|file|mimes:xlsx,xls,csv,jpg,jpeg,png,bmp,doc,docx,pdf,tif,tiff'
        ],[
            'attachments.*.file'=>'The attachment must be a file.',
            'attachments.*.mimes'=>'The attachments must be a file of type: xlsx, xls, csv, jpg, jpeg, png, bmp, doc, docx, pdf, tif, tiff.'
        ]);
        if($validator->fails()){
            $request->flash();

            AuditTrail::create([
                'module'=>'Job Orders',
                'submodule'=>'Create Job Order',
                'action'=>'Validation error on create',
                'created_by'=>\Auth::user()->id
            ]);

            return redirect('job-orders/create')
                    ->withErrors($validator);
        }

        $jo = JobOrder::orderBy('id', 'DESC')->first();
        $project = Incident::where('ir_no', $request->input('ir_no'))->first();

        if(!$jo){
            $jo = new \stdClass();
            $jo->id = 1;
        }else{
            $jo->id += 1;
        }

        $pre = '';
        for($i = 4; $i > strlen($jo->id); $i--){
            $pre .= '0';
        }

        JobOrder::create([
            'jo_no'=>'JO-' . $pre . $jo->id,
            'ir_no'=>$project->id,
            'project_id'=>$project->project_id,
            'priority_level'=>$request->input('priority_level'),
            'asset_description'=>$request->input('asset_description'),
            'brief_description'=>$request->input('brief_description'),
            'details'=>$request->input('details'),
            'remarks'=>$request->input('remarks'),
            'schedule'=>$request->input('schedule'),
            'issued_by'=>Auth::user()->id
        ]);

        if($project->status == 'Open' || $project->status == 'Reopen'){
            $project->status = 'In Progress';
            $project->save();
        }

        $files = $request->file('attachments');

        if(!empty($files) && \Gate::allows('access-matrix', 14)){
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $ext = $file->extension();
                $fnameNoExt = pathinfo($filename,PATHINFO_FILENAME);
                $path = storage_path("app\\public\\o&m\\" . $filename);
                if (\File::exists($path)) {
                    $i = 1;
                    while(\File::exists(storage_path("app\\public\\o&m\\" . $fnameNoExt . '('. $i .').' . $ext))) $i++;
                    $filename = $fnameNoExt . '(' . $i . ').' . $ext;
                }
                Storage::disk('local')->put('public/o&m/'.$filename, file_get_contents($file->getRealPath()));

                JobOrderUpload::create([
                    'jo_no'=>$jo->id,
                    'filename'=>$filename
                ]);
            }
        }

        AuditTrail::create([
            'module'=>'Job Orders',
            'submodule'=>'Create Job Orders',
            'action'=>'Created JO-' . $pre . $jo->id,
            'created_by'=>\Auth::user()->id
        ]);

        return redirect('/job-orders')->with('jobOrderCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(\Gate::denies('access-matrix', 11)){
            return redirect('/');
        }
        if(isset($_GET['type'])){
            $jobOrder = JobOrder::join('incident_details', 'job_order_details.ir_no', 'incident_details.id')->join('project_details', 'incident_details.project_id', 'project_details.id')->where('job_order_details.jo_no', $id)->select('project_details.project_name AS project_name', 'project_details.location AS location', 'project_details.plant_id AS plant_id', 'project_details.dc_capacity AS capacity', 'job_order_details.priority_level AS priority_level', 'job_order_details.asset_description AS asset_description', 'incident_details.ir_no', 'job_order_details.jo_no', 'job_order_details.schedule', 'job_order_details.brief_description', 'job_order_details.details', 'job_order_details.remarks', 'job_order_details.id')->firstOrFail();
            $file = JobOrderUpload::where('jo_no', $jobOrder->id)->get();
            $jobOrder->file = $file;
            return $jobOrder;
        }

        $jobOrder = JobOrder::
        join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
        ->join('project_details', 'incident_details.project_id', 'project_details.id')
        ->join('tjsg_hris.employee_details', 'incident_details.prepared_by', 'tjsg_hris.employee_details.id')
        ->where('job_order_details.id', $id)
        ->select('job_order_details.id', 'incident_details.ir_no', 'project_details.project_name', 'project_details.location', 'project_details.plant_id', 'project_details.dc_capacity', 'job_order_details.jo_no', 'job_order_details.schedule', 'job_order_details.asset_description', 'job_order_details.priority_level', 'job_order_details.brief_description', 'job_order_details.details', 'job_order_details.remarks')->firstOrFail();

        $file = JobOrderUpload::where(['jo_no'=>$id, 'deleted'=>0])->get();

        AuditTrail::create([
            'module'=>'Job Orders',
            'submodule'=>'View Job Orders',
            'action'=>'Viewed ' . $jobOrder->jo_no,
            'created_by'=>\Auth::user()->id
        ]);

        return view('pages.createJobOrder')->with('showJobOrder', $jobOrder)->with('file', $file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(\Gate::denies('access-matrix', 12) || \Gate::denies('update-job-order', $id)){
            return redirect('/');
        }
        $jobOrder = JobOrder::
        join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
        ->join('project_details', 'incident_details.project_id', 'project_details.id')
        ->join('tjsg_hris.employee_details', 'incident_details.prepared_by', 'tjsg_hris.employee_details.id')
        ->where('job_order_details.id', $id)
        ->select('job_order_details.id', 'incident_details.ir_no', 'project_details.project_name', 'project_details.location', 'project_details.plant_id', 'project_details.dc_capacity', 'job_order_details.jo_no', 'job_order_details.schedule', 'job_order_details.asset_description', 'job_order_details.priority_level', 'job_order_details.brief_description', 'job_order_details.details', 'job_order_details.remarks')->firstOrFail();

        $file = JobOrderUpload::where(['jo_no'=>$id, 'deleted'=>0])->get();

        return view('pages.createJobOrder')->with('updateJobOrder', $jobOrder)->with('file', $file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(\Gate::denies('access-matrix', 12) && \Gate::denies('update-job-order', $id)){
            return redirect('/');
        }
        $jo = JobOrder::where('id', $id)->firstOrFail();
        if(Auth::user()->isUserType() == 'Maintenance'){
            $this->validate($request, [
                'schedule'=>'nullable|date'
            ]);

            JobOrder::where('id', $id)->update([
                'schedule'=>$request->input('schedule')
            ]);

            AuditTrail::create([
                'module'=>'Job Orders',
                'submodule'=>'Update Job Orders',
                'action'=>'Updated ' . $jo->jo_no,
                'created_by'=>\Auth::user()->id
            ]);

            return view('pages.jobOrders')->with('jobOrderUpdated', true);
        }else{
            $this->validate($request, [
                'ir_no'=>'required|exists:incident_details,ir_no',
                'asset_description'=>'required|max:300',
                'priority_level'=>'required|in:Normal,High',
                'schedule'=>'nullable|date',
                'brief_description'=>'required|max:300',
                'details'=>'required|max:300',
                'remarks'=>'required|max:300'
            ]);

            $project = Incident::where('ir_no', $request->input('ir_no'))->first();

            JobOrder::where('id', $id)->update([
                'ir_no'=>$project->id,
                'priority_level'=>$request->input('priority_level'),
                'asset_description'=>$request->input('asset_description'),
                'brief_description'=>$request->input('brief_description'),
                'details'=>$request->input('details'),
                'remarks'=>$request->input('remarks'),
                'schedule'=>$request->input('schedule'),
                'issued_by'=>Auth::user()->id
            ]);

            // TODO: Upload of attachments and insert to database
            $files = $request->file('attachments');

            if(!empty($files) && \Gate::allows('access-matrix', 14)){
                foreach($files as $file){
                    $filename = $file->getClientOriginalName();
                    $ext = $file->extension();
                    $fnameNoExt = pathinfo($filename,PATHINFO_FILENAME);
                    $path = storage_path("app\\public\\o&m\\" . $filename);
                    if (\File::exists($path)) {
                        $i = 1;
                        while(\File::exists(storage_path("app\\public\\o&m\\" . $fnameNoExt . '('. $i .').' . $ext))) $i++;
                        $filename = $fnameNoExt . '(' . $i . ').' . $ext;
                    }
                    Storage::disk('local')->put('public/o&m/'.$filename, file_get_contents($file->getRealPath()));

                    JobOrderUpload::create([
                        'jo_no'=>$id,
                        'filename'=>$filename
                    ]);
                }
            }

            AuditTrail::create([
                'module'=>'Job Orders',
                'submodule'=>'Update Job Orders',
                'action'=>'Updated ' . $jo->jo_no,
                'created_by'=>\Auth::user()->id
            ]);

            return redirect('/job-orders')->with('jobOrderUpdated', true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->input('type') && $request->input('type') == 'deleteAttachment'){
            if(\Gate::allows('delete-job-order-attachment', $id)){
                $jo = JobOrderUpload::where('id', $id)->update([
                    'deleted'=>1
                ]);

                if($jo){
                    AuditTrail::create([
                        'module'=>'Job Orders',
                        'submodule'=>'Attachments',
                        'action'=>'Deleted Attachment ID: ' . $id,
                        'created_by'=>\Auth::user()->id
                    ]);
                    return ['title'=>'Deleted', 'msg'=>'Attachment deleted successfully', 'status'=>'success'];
                }else{
                    \Log::alert('Error in deleting attachment', ['Attachment'=>$jo]);
                    return ['title'=>'Error', 'msg'=>'There was an error in deleting the attachment', 'status'=>'error'];
                }
            }
        }

        $checkFar = JobOrder::join('field_activity_details', 'job_order_details.id', 'field_activity_details.jo_no')->where('job_order_details.id', $id)->first();

        if($checkFar){
            return ['title'=>'Error', 'msg'=>'The Job Order you are trying to delete has an existing Field Activity Report.', 'type'=>'error'];
        }

        if(\Gate::denies('access-matrix', 13) || \Gate::denies('delete-job-order', $id)){
            \Log::alert('Gate denied deletion of job order report.', ['user id: '=>\Auth::user()->id, 'jo_no'=>$id]);
            return ['title'=>'Error', 'msg'=>'An error occured when trying to delete your request.', 'type'=>'error'];
        }

        $jo = JobOrder::where('id', $id)->update([
            'isDeleted'=>1
        ]);

        if($jo){
            $jo = JobOrder::where('id', $id)->first();
            AuditTrail::create([
                'module'=>'Job Orders',
                'submodule'=>'Delete Job Orders',
                'action'=>'Deleted ' . $jo->jo_no,
                'created_by'=>\Auth::user()->id
            ]);

            return ['title'=>'Deleted', 'msg'=>'Job Order deleted successfully.', 'type'=>'success'];
        }else{
            \Log::alert('Error on deleting job order.', ['Job Order'=>$jo]);
            return ['title'=>'Error', 'msg'=>'An error occured when trying to delete your request.', 'type'=>'error'];
        }

        abort(404);
    }

    public function previewPdf($id){
        if(\Gate::denies('access-matrix', 15)){
            return redirect('/');
        }
        $jobOrder = JobOrder::join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
        ->join('project_details', 'incident_details.project_id', 'project_details.id')
        ->select('job_order_details.jo_no', 'job_order_details.created_at', 'job_order_details.asset_description', 'job_order_details.priority_level', 'job_order_details.brief_description', 'job_order_details.details', 'job_order_details.remarks', 'job_order_details.schedule', 'project_details.project_name', 'project_details.plant_id', 'project_details.location', 'project_details.dc_capacity')
        ->where('job_order_details.id', $id)->firstOrFail();
        $jobOrder->date_today = date('m/d/Y');
        $pdf = \PDF::loadView('pdf.joborder', compact('jobOrder'))->setPaper('a4', 'portrait');
        AuditTrail::create([
            'module'=>'Job Orders',
            'submodule'=>'Preview PDF',
            'action'=>'Previewed PDF for ' . $jobOrder->jo_no,
            'created_by'=>\Auth::user()->id
        ]);
        return $pdf->stream();
    }

    public function downloadPdf($id){
        if(\Gate::denies('access-matrix', 15)){
            return redirect('/');
        }
        $jobOrder = JobOrder::join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
       ->join('project_details', 'incident_details.project_id', 'project_details.id')
       ->select('job_order_details.jo_no', 'job_order_details.created_at', 'job_order_details.asset_description', 'job_order_details.priority_level', 'job_order_details.brief_description', 'job_order_details.details', 'job_order_details.remarks', 'job_order_details.schedule', 'project_details.project_name', 'project_details.plant_id', 'project_details.location', 'project_details.dc_capacity')
       ->where('job_order_details.id', $id)->firstOrFail();
       $jobOrder->date_today = date('m/d/Y');
        $pdf = \PDF::loadView('pdf.joborder', compact('jobOrder'))->setPaper('a4', 'portrait');
        AuditTrail::create([
            'module'=>'Job Orders',
            'submodule'=>'Update Job Orders',
            'action'=>'Downloaded PDF for ' . $jobOrder->jo_no,
            'created_by'=>\Auth::user()->id
        ]);
        return $pdf->download($jobOrder->jo_no . '.pdf');
    }
}
