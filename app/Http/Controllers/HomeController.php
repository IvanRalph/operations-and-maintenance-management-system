<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        session([
            'ip'=>getHostByName(getHostName())
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::denies('access-matrix', 1)){
            if(\Gate::denies('access-matrix', 4)){
                if(\Gate::denies('access-matrix', 11)){
                    if(\Gate::denies('access-matrix', 18)){
                        return redirect('summary/incidents');
                    }else{
                        return redirect('/field-activities');
                    }
                }else{
                    return redirect('/job-orders');
                }
            }else{
                return redirect('/incidents');
            }
        }
        $incident = Incident::select(\DB::raw('COUNT(case status when "Open" then 1 else null end) AS open'), \DB::raw('COUNT(case status when "Reopen" then 1 else null end) AS reopen'), \DB::raw('COUNT(case status when "In Progress" then 1 else null end) AS in_progress'), \DB::raw('COUNT(case status when "Closed" then 1 else null end) AS closed'), \DB::raw('COUNT(case status when "Unresolved" then 1 else null end) AS unresolved'))->first();
        return view('/home')->with('incident', $incident);
    }
}
