<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;
use App\JobOrder;
use App\FieldActivity;
use App\Project;
use App\Employee;
use App\AuditTrail;
use DataTables;

class DatatableController extends Controller
{
    public function incident(){
    	$builder = Incident::query()
    	->join('project_details', 'incident_details.project_id', 'project_details.id')
    	->join('tjsg_hris.employee_details', 'incident_details.prepared_by', 'tjsg_hris.employee_details.id')
    	->select('incident_details.id', 'incident_details.ir_no', 'incident_details.created_at', 'incident_details.updated_at', 'project_details.project_name', 'project_details.location AS location', 'project_details.plant_id', 'project_details.dc_capacity', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = incident_details.prepared_by) AS prepared_by'), 'incident_details.status', 'incident_details.status_date', 'incident_details.brief_description', 'incident_details.incident_details', 'incident_details.other_comments')
        ->where('status', '!=', 'Deleted')
    	->orderBy('created_at', 'DESC');
    	return datatables()->eloquent($builder)->toJson();
    }

    public function jobOrder(){
    	$builder = JobOrder::query()
    	->join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
    	->join('project_details', 'incident_details.project_id', 'project_details.id')
    	->join('tjsg_hris.employee_details', 'incident_details.prepared_by', 'tjsg_hris.employee_details.id')
        ->where('job_order_details.isDeleted', 0)
    	->select('job_order_details.id', 'job_order_details.jo_no', 'job_order_details.created_at', 'job_order_details.updated_at', 'incident_details.ir_no', 'project_details.project_name', 'project_details.location', 'project_details.plant_id', 'project_details.dc_capacity', 'job_order_details.asset_description', 'incident_details.brief_description', 'job_order_details.priority_level', 'job_order_details.details', 'job_order_details.remarks', 'job_order_details.schedule', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = job_order_details.issued_by) AS issued_by'))
        ->orderBy('job_order_details.created_at', 'DESC');
    	return datatables()->eloquent($builder)->toJson();
    }

    public function fieldActivity(){
    	$builder = FieldActivity::query()
    	->join('job_order_details', 'field_activity_details.jo_no', 'job_order_details.id')
    	->join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
    	->join('project_details', 'incident_details.project_id', 'project_details.id')
    	->join('tjsg_hris.employee_details', 'field_activity_details.in_charge', 'tjsg_hris.employee_details.id')
        ->where('field_activity_details.isDeleted', 0)
    	->select('field_activity_details.id', 'field_activity_details.far_no', 'field_activity_details.created_at', 'field_activity_details.updated_at', 'job_order_details.jo_no', 'project_details.project_name', 'project_details.dc_capacity', 'field_activity_details.issue_description', 'field_activity_details.observation', 'field_activity_details.actions', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = field_activity_details.in_charge) AS in_charge'), 'incident_details.ir_no', 'project_details.location', 'job_order_details.asset_description', 'field_activity_details.remarks')
        ->orderBy('field_activity_details.created_at', 'DESC');
    	return datatables()->eloquent($builder)->toJson();
    }

    public function project(){
        $builder = Project::query()
        ->leftJoin('tjsg_hris.employee_details', function($join){
            $join->on('project_details.created_by', 'tjsg_hris.employee_details.id');
            $join->on('project_details.updated_by', 'tjsg_hris.employee_details.id');
        })
        ->where('project_details.isDeleted', 0)
        ->select('project_details.id', 'project_name', 'plant_id', 'dc_capacity', 'ac_capacity', 'location', 'date_commissioned', 'pv_modules', 'inverter', 'communication', 'created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = project_details.created_by) AS created_by'), 'updated_at',  \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = project_details.updated_by) AS updated_by'), 'project_details.plant_type');
        return datatables()->eloquent($builder)->toJson();
    }

    public function user(){
        $builder = Employee::query()
        ->leftJoin('eqrr_incidentmonitoring.user_access_details', 'employee_details.id', 'user_access_details.employee_id')
        ->join('department_details', 'employee_details.department_id', 'department_details.department_id')
        ->leftJoin('eqrr_incidentmonitoring.user_types', 'user_access_details.user_type_id', 'user_types.id')
        ->select('employee_details.id AS id', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM employee_details ed WHERE employee_id = employee_details.employee_id) AS full_name'), 'department_details.name AS department', 'user_types.user_type', 'user_access_details.updated_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM employee_details WHERE id = user_access_details.updated_by) AS updated_by'));
        return datatables()->eloquent($builder)->toJson();
    }

    public function auditTrail(){
        $builder = AuditTrail::query()
        ->join('tjsg_hris.employee_details', 'audit_trails.created_by', 'employee_details.id')
        ->leftJoin('tjsg_hris.employee_details AS ed', 'audit_trails.updated_by', 'ed.id')
        ->select('audit_trails.module', 'audit_trails.submodule', 'audit_trails.action', 'audit_trails.created_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = audit_trails.created_by) AS created_by'), 'audit_trails.updated_at', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = audit_trails.updated_by) AS updated_by'), 'audit_trails.id')->orderBy('audit_trails.created_at', 'DESC');
        return datatables()->eloquent($builder)->toJson();
    }
}
