<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function incidents(){
    	return view('pages.summaryIncidents');
    }

    public function jobOrders(){
    	return view('pages.summaryJobOrders');
    }

    public function fieldActivities(){
    	return view('pages.summaryFieldActivities');
    }
}
