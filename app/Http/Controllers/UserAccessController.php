<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\UserAccess;
use App\AuditTrail;

class UserAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.setupUserAccess');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.setupUserAccessCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::
        leftJoin('eqrr_incidentmonitoring.user_access_details', 'employee_details.id', 'user_access_details.employee_id')
        ->leftJoin('eqrr_incidentmonitoring.user_types', 'user_access_details.user_type_id', 'user_types.id')
        ->leftJoin('department_details', 'employee_details.department_id', 'department_details.department_id')
        ->select(\DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM employee_details ed WHERE employee_id = employee_details.employee_id) AS full_name'), 'department_details.name AS department', 'user_access_details.user_type_id AS user_type_id')
        ->where('employee_details.id', $id)->firstOrFail();

        AuditTrail::create([
            'module'=>'User Access',
            'submodule'=>'View User Access',
            'action'=>'Viewed ' . $employee->full_name . '\'s access',
            'created_by'=>\Auth::user()->id
        ]);
        return view('pages.setupUserAccessCreate')->with('employee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::
        leftJoin('eqrr_incidentmonitoring.user_access_details', 'employee_details.id', 'user_access_details.employee_id')
        ->leftJoin('eqrr_incidentmonitoring.user_types', 'eqrr_incidentmonitoring.user_access_details.user_type_id', 'user_types.id')
        ->leftJoin('department_details', 'employee_details.department_id', 'department_details.department_id')
        ->select(\DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM employee_details ed WHERE employee_id = employee_details.employee_id) AS full_name'), 'department_details.name AS department', 'user_access_details.user_type_id AS user_type_id', 'employee_details.id AS employee_id')
        ->where('employee_details.id', $id)->firstOrFail();
        $employee->type = 'edit';
        return view('pages.setupUserAccessCreate')->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_type'=>'required|in:1,2'
        ]);

        $employee = Employee::where('id', $id)->firstOrFail();

        $userAccess = UserAccess::updateOrCreate(
            ['employee_id'=>$employee->id],
            ['user_type_id'=>$request->input('user_type'), 'updated_by'=>\Auth::user()->id]
        );

        AuditTrail::create([
            'module'=>'User Access',
            'submodule'=>'Update User Access',
            'action'=>'Updated Employee ' . $employee->employee_id,
            'created_by'=>\Auth::user()->id
        ]);
        return view('pages.setupUserAccess')->with('userUpdated', $employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
