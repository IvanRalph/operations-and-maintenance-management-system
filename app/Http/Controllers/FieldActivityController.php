<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\FieldActivity;
use App\JobOrder;
use App\FieldActivityUpload;
use App\Incident;
use App\IncidentStatus;
use App\AuditTrail;
use Storage;

class FieldActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::denies('access-matrix', 18)){
            return redirect('/');
        }
        return view('pages.fieldActivity');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(\Gate::denies('access-matrix', 17)){
            return redirect('field-activities');
        }
        return view('pages.fieldActivityReportCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(\Gate::denies('access-matrix', 17)){
            return redirect('field-activities');
        }

        $validator = \Validator::make($request->all(), [
            'jo_no'=>'required|unique:field_activity_details,jo_no|exists:job_order_details,jo_no',
            'issue_description'=>'required|max:300',
            'observation'=>'required|max:300',
            'actions'=>'required|max:300',
            'remarks'=>'nullable|max:300',
            'attachments.*' => 'nullable|file|mimes:xlsx,xls,csv,jpg,jpeg,png,bmp,doc,docx,pdf,tif,tiff'
        ],[
            'attachments.*.file'=>'The attachment must be a file.',
            'attachments.*.mimes'=>'The attachments must be a file of type: xlsx, xls, csv, jpg, jpeg, png, bmp, doc, docx, pdf, tif, tiff.'
        ]);
        if($validator->fails()){
            $request->flash();
            return redirect('field-activities/create')
                    ->withErrors($validator);
        }

        $far = FieldActivity::orderBy('id', 'DESC')->first();
        $jo = JobOrder::where('jo_no', $request->input('jo_no'))->first();

        if(!$far){
            $far = new \stdClass();
            $far->id = 1;
        }else{
            $far->id += 1;
        }

        $pre = '';
        for($i = 4; $i > strlen($far->id); $i--){
            $pre .= '0';
        }

        FieldActivity::create([
            'far_no'=>'FAR-'.$pre.$far->id,
            'jo_no'=>$jo->id,
            'issue_description'=>$request->input('issue_description'),
            'observation'=>$request->input('observation'),
            'actions'=>$request->input('actions'),
            'remarks'=>$request->input('remarks'),
            'in_charge'=>Auth::user()->id
        ]);

        $files = $request->file('attachments');

        if(!empty($files) && \Gate::allows('access-matrix', 21)){
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $ext = $file->extension();
                $fnameNoExt = pathinfo($filename,PATHINFO_FILENAME);
                $path = storage_path("app\\public\\o&m\\" . $filename);
                if (\File::exists($path)) {
                    $i = 1;
                    while(\File::exists(storage_path("app\\public\\o&m\\" . $fnameNoExt . '('. $i .').' . $ext))) $i++;
                    $filename = $fnameNoExt . '(' . $i . ').' . $ext;
                }
                Storage::disk('local')->put('public/o&m/'.$filename, file_get_contents($file->getRealPath()));

                FieldActivityUpload::create([
                    'far_no'=>$far->id,
                    'filename'=>$filename
                ]);
            }
        }

        Incident::where('id', $jo->ir_no)->update([
            'status'=>'Resolved'
        ]);

        IncidentStatus::create([
            'ir_no'=>$jo->ir_no,
            'status'=>'Resolved',
            'updated_by'=>\Auth::user()->id
        ]);

        AuditTrail::create([
            'module'=>'Field Activities',
            'submodule'=>'Create Field Activities',
            'action'=>'Created FAR-'.$pre.$far->id,
            'created_by'=>\Auth::user()->id
        ]);

        return redirect('/field-activities')->with('fieldActivityCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(\Gate::denies('access-matrix', 18)){
            return redirect('/');
        }
        $far = FieldActivity::join('job_order_details', 'field_activity_details.jo_no', 'job_order_details.id')
        ->join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
        ->join('project_details', 'incident_details.project_id', 'project_details.id')
        ->select('field_activity_details.id', 'field_activity_details.far_no', 'field_activity_details.issue_description', 'field_activity_details.observation', 'field_activity_details.actions', 'field_activity_details.remarks', 'job_order_details.jo_no', 'job_order_details.asset_description', 'job_order_details.priority_level', 'project_details.dc_capacity', 'project_details.project_name', 'project_details.plant_id', 'project_details.location')
        ->where('field_activity_details.id', $id)->firstOrFail();

        $file = FieldActivityUpload::where(['far_no'=>$id, 'deleted'=>0])->get();

        AuditTrail::create([
            'module'=>'Field Activities',
            'submodule'=>'View Field Activities',
            'action'=>'Viewed ' .$far->far_no,
            'created_by'=>\Auth::user()->id
        ]);

        return view('pages.fieldActivityReportCreate')->with('showFieldActivity', $far)->with('file', $file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(\Gate::denies('update-field-activity') || \Gate::denies('access-matrix', 19)){
            return redirect('field-activities');
        }

        $far = FieldActivity::join('job_order_details', 'field_activity_details.jo_no', 'job_order_details.id')
        ->join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
        ->join('project_details', 'incident_details.project_id', 'project_details.id')
        ->select('field_activity_details.id', 'field_activity_details.far_no', 'field_activity_details.issue_description', 'field_activity_details.observation', 'field_activity_details.actions', 'field_activity_details.remarks', 'job_order_details.jo_no', 'job_order_details.asset_description', 'job_order_details.priority_level', 'project_details.dc_capacity', 'project_details.project_name', 'project_details.plant_id', 'project_details.location', 'field_activity_details.in_charge')
        ->where('field_activity_details.id', $id)->firstOrFail();

        $file = FieldActivityUpload::where(['far_no'=>$id, 'deleted'=>0])->get();
        return view('pages.fieldActivityReportCreate')->with('updateFieldActivity', $far)->with('file', $file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function update(Request $request, $id)
    {
        if(\Gate::denies('update-field-activity') || \Gate::denies('access-matrix', 19)){
            return redirect('field-activities');
        }

        $this->validate($request, [
            'jo_no'=>'required|exists:job_order_details,jo_no',
            'issue_description'=>'required|max:300',
            'observation'=>'required|max:300',
            'actions'=>'required|max:300',
            'remarks'=>'nullable|max:300'
        ]);

        $jo = JobOrder::where('jo_no', $request->input('jo_no'))->first();

        $far = FieldActivity::where('id', $id)->update([
            'jo_no'=>$jo->id,
            'issue_description'=>$request->input('issue_description'),
            'observation'=>$request->input('observation'),
            'actions'=>$request->input('actions'),
            'remarks'=>$request->input('remarks')
        ]);

        $files = $request->file('attachments');

        if(!empty($files) && \Gate::allows('access-matrix', 21)){
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $ext = $file->extension();
                $fnameNoExt = pathinfo($filename,PATHINFO_FILENAME);
                $path = storage_path("app\\public\\o&m\\" . $filename);
                if (\File::exists($path)) {
                    $i = 1;
                    while(\File::exists(storage_path("app\\public\\o&m\\" . $fnameNoExt . '('. $i .').' . $ext))) $i++;
                    $filename = $fnameNoExt . '(' . $i . ').' . $ext;
                }
                Storage::disk('local')->put('public/o&m/'.$filename, file_get_contents($file->getRealPath()));

                FieldActivityUpload::create([
                    'far_no'=>$id,
                    'filename'=>$filename
                ]);
            }
        }

        $far = FieldActivity::where('id', $id)->first();

        AuditTrail::create([
            'module'=>'Field Activities',
            'submodule'=>'Update Field Activities',
            'action'=>'Updated ' .$far->far_no,
            'created_by'=>\Auth::user()->id
        ]);

        return redirect('/field-activities')->with('fieldActivityUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fa = FieldActivity::where('id', $id)->first();
        if(!$fa){
            \Log::notice('Report doesn\'t exist.', ['report'=>$fa, 'user'=>\Auth::user()->id]);
            return ['title'=>'Error', 'msg'=>'The report you are trying delete doesn\'t exist. If you think otherwise, please contact it.enterprise@solarphilippines.ph.', 'type'=>'error'];
        }

        if(\Gate::denies('delete-field-activity', $fa) || \Gate::denies('access-matrix', 20)){
            \Log::notice('User has no ability to delete report');
            return ['title'=>'Error', 'msg'=>'There was an error in deleting your request.', 'type'=>'error'];
        }

        FieldActivity::where('id', $id)->update([
            'isDeleted'=>1
        ]);

        AuditTrail::create([
            'module'=>'Field Activities',
            'submodule'=>'Delete Field Activities',
            'action'=>'Deleted ' .$fa->far_no,
            'created_by'=>\Auth::user()->id
        ]);

        return ['title'=>'Deleted', 'msg'=>'Field Activity Report deleted successfully.', 'type'=>'success'];
    }

    public function previewPdf($id){
        if(\Gate::denies('access-matrix', 22)){
            return redirect('/');
        }
       $fieldActivity = FieldActivity::join('job_order_details', 'field_activity_details.jo_no', 'job_order_details.id')
       ->join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
       ->join('project_details', 'incident_details.project_id', 'project_details.id')
       ->select('field_activity_details.far_no', 'field_activity_details.created_at', 'field_activity_details.issue_description', 'field_activity_details.observation', 'field_activity_details.actions', 'field_activity_details.remarks', 'project_details.project_name', 'project_details.location', 'project_details.dc_capacity', 'job_order_details.jo_no', 'job_order_details.asset_description')
       ->where('field_activity_details.id', $id)->firstOrFail();
       $fieldActivity->date_today = date('m/d/Y');
        $pdf = \PDF::loadView('pdf.fieldactivity', compact('fieldActivity'))->setPaper('a4', 'portrait');

        AuditTrail::create([
            'module'=>'Field Activities',
            'submodule'=>'View PDF',
            'action'=>'Viewed PDF for-' .$fieldActivity->far_no,
            'created_by'=>\Auth::user()->id
        ]);
        return $pdf->stream();
    }

    public function downloadPdf($id){
        if(\Gate::denies('access-matrix', 22)){
            return redirect('/');
        }
        $fieldActivity = FieldActivity::join('job_order_details', 'field_activity_details.jo_no', 'job_order_details.id')
       ->join('incident_details', 'job_order_details.ir_no', 'incident_details.id')
       ->join('project_details', 'incident_details.project_id', 'project_details.id')
       ->select('field_activity_details.far_no', 'field_activity_details.created_at', 'field_activity_details.issue_description', 'field_activity_details.observation', 'field_activity_details.actions', 'field_activity_details.remarks', 'project_details.project_name', 'project_details.location', 'project_details.dc_capacity', 'job_order_details.jo_no', 'job_order_details.asset_description')
       ->where('field_activity_details.id', $id)->firstOrFail();
       $fieldActivity->date_today = date('m/d/Y');
        $pdf = \PDF::loadView('pdf.fieldactivity', compact('fieldActivity'))->setPaper('a4', 'portrait');
        AuditTrail::create([
            'module'=>'Field Activities',
            'submodule'=>'Download PDF',
            'action'=>'Downloaded PDF for-' .$fieldActivity->far_no,
            'created_by'=>\Auth::user()->id
        ]);
        return $pdf->download($fieldActivity->far_no. '.pdf');
    }
}
