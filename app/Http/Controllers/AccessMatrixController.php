<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AccessMatrixSystem;
use App\AuditTrail;

class AccessMatrixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        $acl = AccessMatrixSystem::get();
        $count = AccessMatrixSystem::count();
        return view('pages.setupAccessMatrix')->with('acl', $acl)->with('count', $count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $count = AccessMatrixSystem::count();
        $acl = AccessMatrixSystem::get();
        for ($i=1; $i <= $count; $i++) { 
            AccessMatrixSystem::where('id', $acl[$i-1]->id)->update([
                'operation'=>$request->input('op_'. $i) != null ? 1 : 0,
                'maintenance'=>$request->input('main_'. $i) != null ? 1 : 0
            ]);
        }

        AuditTrail::create([
            'module'=>'Access Matrix',
            'submodule'=>'Update access',
            'action'=>'Updated Access Matrix',
            'created_by'=>\Auth::user()->id
        ]);

        return redirect('access-matrix')->with('accessUpdated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
