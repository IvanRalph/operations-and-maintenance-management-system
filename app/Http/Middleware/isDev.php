<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class isDev
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $a = User::where('employee_id', session('employee_id'))->first();
        if ($a && $a->email == 'ralph.vitto@solarphilippines.ph') {
                return $next($request);
         }

         abort(404);
    }
}
