<?php

namespace App\Http\Middleware;

use Closure;
use App\UserAccess;
use Illuminate\Support\Facades\Auth;

class userTypes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $employee_id = Auth::user()->employee_id;
        $userAccess = UserAccess::
        join('tjsg_hris.employee_details', 'user_access_details.employee_id', 'employee_details.id')
        ->join('user_types', 'user_access_details.user_type_id', 'user_types.id')
        ->where('employee_details.employee_id', $employee_id)
        ->select('user_types.user_type')
        ->firstOrFail();

        if($userAccess->user_type == $role){
            return $next($request);
        }

        abort(404);
    }
}
