<?php

namespace App\Http\Middleware;

use Closure;
use App\UserAccess;

class access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $ops, $main = 0)
    {
        $a = UserAccess::where('employee_id',\Auth::user()->id)->whereIn('user_type_id', [$ops, $main])->first();
        if($a){
            return $next($request);
        }

        return redirect('/');
    }
}
