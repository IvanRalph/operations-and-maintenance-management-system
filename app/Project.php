<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project_details';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['project_name', 'plant_id', 'plant_type', 'dc_capacity', 'ac_capacity', 'location', 'date_commissioned', 'pv_modules', 'inverter', 'communication', 'created_by', 'updated_by'];
}
