<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{
    protected $table = 'user_access_details';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['employee_id', 'user_type_id', 'updated_by'];
}
