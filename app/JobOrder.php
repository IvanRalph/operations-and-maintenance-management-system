<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrder extends Model
{
    protected $table = 'job_order_details';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['jo_no', 'ir_no', 'priority_level', 'asset_description', 'schedule', 'issued_by', 'brief_description', 'details', 'remarks', 'status'];
}
