<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldActivity extends Model
{
   	protected $table = 'field_activity_details';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['far_no', 'jo_no', 'issue_description', 'observation', 'actions', 'remarks', 'in_charge'];
}
