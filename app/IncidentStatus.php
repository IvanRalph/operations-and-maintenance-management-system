<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentStatus extends Model
{
    protected $table = 'incident_status_tracker';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['ir_no', 'status', 'updated_by'];
}
