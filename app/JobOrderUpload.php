<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrderUpload extends Model
{
	protected $table = 'job_order_upload_details';
	public $primaryKey = 'id';

	public $timestamps = true;

	public $fillable = ['jo_no', 'filename'];
}
