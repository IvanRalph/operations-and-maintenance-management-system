<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentUpload extends Model
{
    protected $table = 'incident_upload_details';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['ir_no', 'filename'];
}
