<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\UserAccess;
use App\Incident;
use App\JobOrder;
use App\FieldActivity;
use App\AccessMatrixSystem;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is-dev', function(){
            $a = User::where('employee_id', session('employee_id'))->first();
            if($a && $a->email == 'ralph.vitto@solarphilippines.ph'){
                return true;
            }else{
                return false;
            }
        });

        Gate::define('create-incident', function($user){
            return $user->isUserType() === 'Maintenance';
        });

        Gate::define('create-field-activity', function($user){
            return $user->isUserType() === 'Maintenance';
        });

        Gate::define('delete-incident', function($user, $id){
            $incident = Incident::where([
                'id'=>$id,
                'prepared_by'=>$user->id,
                'status'=>'Open'
            ])->first();

            if(!$incident){
                return false;
            }else{
                return true;
            }
        });

        Gate::define('create-job-order', function($user){
            return $user->isUserType() === 'Operation';
        });

        Gate::define('delete-job-order', function($user, $id){
            $jo = JobOrder::where([
                'id'=>$id,
                'issued_by'=>$user->id,
                'isDeleted'=>0
            ])->first();

            if(!$jo){
                return false;
            }else{
                return true;
            }
        });

        Gate::define('delete-field-activity', function($user, $id){
            $fa = FieldActivity::join('tjsg_hris.employee_details', 'field_activity_details.in_charge', 'employee_details.id')->first();
            if(!$fa){
                return false;
            } 

            return true;
        });

        Gate::define('update-field-activity', function($user, $id){
            return $user->isUserType() == 'Maintenance' && $id->in_charge == $user->id;
        });

        Gate::define('delete-job-order-attachment', function($user, $id){
            $jo = JobOrder::
            join('job_order_upload_details', 'job_order_details.id', 'job_order_upload_details.jo_no')
            ->where(['job_order_details.issued_by'=>$user->id, 'job_order_upload_details.id'=>$id])
            ->first();

            if(!$jo){
                return false;
            }else{
                return true;
            }
        });

        Gate::define('update-job-order', function($user, $id){
            if($user->isUserType() == 'Operation'){
                $jo = JobOrder::where(['id'=>$id, 'issued_by'=>$user->id])->first();
                if(!$jo){
                    return false;
                }else{
                    return true;
                }
            }else{
                return true;
            }
        });

        Gate::define('delete-incident-attachment', function($user, $id){
            $incident = Incident::
            join('incident_upload_details', 'incident_details.id', 'incident_upload_details.ir_no')
            ->where(['incident_details.prepared_by'=>$user->id, 'incident_upload_details.id'=>$id])
            ->first();

            if(!$incident){
                return false;
            }else{
                return true;
            }
        });

        Gate::define('operations', function(){
            $a = UserAccess::where(['employee_id'=>\Auth::user()->id, 'user_type_id'=>1])->first();
            if($a){
                return true;
            }

            return false;
        });

        Gate::define('maintenance', function(){
            $a = UserAccess::where(['employee_id'=>\Auth::user()->id, 'user_type_id'=>2])->first();
            if($a){
                return true;
            }

            return false;
        });

        Gate::define('delete-project', function($user, $proj){
           return $proj->created_by === $user->id; 
        });

        Gate::define('update-field-activity', function($user){
            return $user->isUserType() === 'Maintenance';
        });

        Gate::define('access-matrix', function($user, $id){
            $acl = AccessMatrixSystem::where('id', $id)->first();
            if($user->isUserType() == 'Operation'){
                return $acl->operation === 1;
            }else{
                return $acl->maintenance === 1;
            }
        });
    }
}
