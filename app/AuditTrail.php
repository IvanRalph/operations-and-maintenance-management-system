<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
    protected $table = 'audit_trails';
    public $primaryKey = 'id';

    public $timestamps = true;

    public $fillable = ['module', 'submodule', 'action', 'created_by', 'updated_by'];
}
