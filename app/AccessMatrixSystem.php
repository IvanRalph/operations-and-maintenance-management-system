<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessMatrixSystem extends Model
{
    protected $table = 'access_matrix_systems';
    public $primaryKey = 'id';

    public $timestamps = true;
}
