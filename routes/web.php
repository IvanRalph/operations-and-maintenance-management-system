<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware'=>['guest', 'revalidate']], function(){
	Route::post('/validate_login', ['as'=>'validate_login', 'uses'=>'Auth\LoginController@validate_login']);

	Route::post('/login', ['as'=>'login', 'uses'=>'Auth\LoginController@login']);
});

Route::get('test', function(){
	abort(403, 'Auth error');
});

Route::group(['middleware'=>['auth', 'revalidate']], function(){
	Route::group(['middleware'=>'access:2'], function(){
		Route::resource('projects', 'ProjectController');

		Route::resource('access-matrix', 'AccessMatrixController');

		Route::resource('audit-trail', 'AuditTrailController');

		Route::resource('users', 'UserAccessController');
	});

	Route::group(['middleware'=>['access:1,2']], function(){
		Route::resource('projects', 'ProjectController', ['only' => ['show']]);

		Route::resource('incidents', 'IncidentController');

		Route::resource('job-orders', 'JobOrderController');

		Route::resource('field-activities', 'FieldActivityController');

		Route::get('summary/incidents', 'PageController@incidents');

		Route::get('summary/job-orders', 'PageController@jobOrders');

		Route::get('summary/field-activities', 'PageController@fieldActivities');

		Route::get('/autocomplete/project', 'SelectController@project');

		Route::get('/autocomplete/incident', 'SelectController@incident');

		Route::get('/autocomplete/job-order', 'SelectController@jobOrder');

		Route::get('admin/logs', ['middleware'=>'dev', 'uses'=>'\Rap2hpoutre\LaravelLogViewer\LogViewerController@index']);

		Route::get('datatable/incidentData', 'DatatableController@incident');

		Route::get('datatable/jobOrderData', 'DatatableController@jobOrder');

		Route::get('datatable/fieldActivityData', 'DatatableController@fieldActivity');

		Route::get('datatable/projectData', 'DatatableController@project');

		Route::get('datatable/userAccessData', 'DatatableController@user');

		Route::get('datatable/auditTrailData', 'DatatableController@auditTrail');

		Route::get('incidents/storage/{filename}', 'FileController@storage');

		Route::get('job-orders/storage/{filename}', 'FileController@storage');

		Route::get('field-activities/storage/{filename}', 'FileController@storage');

		Route::get('incidents/previewPdf/{id}', 'IncidentController@previewPdf');

		Route::get('incidents/downloadPdf/{id}', 'IncidentController@downloadPdf');

		Route::get('job-orders/previewPdf/{id}', 'JobOrderController@previewPdf');

		Route::get('job-orders/downloadPdf/{id}', 'JobOrderController@downloadPdf');

		Route::get('field-activities/previewPdf/{id}', 'FieldActivityController@previewPdf');

		Route::get('field-activities/downloadPdf/{id}', 'FieldActivityController@downloadPdf');
	});
});