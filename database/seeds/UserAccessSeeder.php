<?php

use Illuminate\Database\Seeder;

class UserAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_access_details')->insert([
        	[
        		'employee_id'=>3,
        		'user_type_id'=>2,
        		'updated_by'=>3
        	]
        ]);
    }
}
