<?php

use Illuminate\Database\Seeder;

class AccessMatrixSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('access_matrix_systems')->insert([
        	[
        		'module'=>'Dashboard',
        		'submodule'=>'Dashboard',
        		'action'=>'Read',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Incident',
        		'submodule'=>'Incident',
        		'action'=>'Search',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Incident',
        		'submodule'=>'Incident',
        		'action'=>'Create',
        		'operation'=>0,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Incident',
        		'submodule'=>'Incident',
        		'action'=>'Read',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Incident',
        		'submodule'=>'Incident',
        		'action'=>'Update',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Incident',
        		'submodule'=>'Incident',
        		'action'=>'Delete',
        		'operation'=>0,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Incident',
        		'submodule'=>'Incident',
        		'action'=>'Upload',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Incident',
        		'submodule'=>'Incident',
        		'action'=>'Preview & Download PDF',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Job Order',
        		'submodule'=>'Job Order',
        		'action'=>'Search',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Job Order',
        		'submodule'=>'Job Order',
        		'action'=>'Create',
        		'operation'=>1,
        		'maintenance'=>0
        	],
        	[
        		'module'=>'Job Order',
        		'submodule'=>'Job Order',
        		'action'=>'Read',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Job Order',
        		'submodule'=>'Job Order',
        		'action'=>'Update',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Job Order',
        		'submodule'=>'Job Order',
        		'action'=>'Delete',
        		'operation'=>1,
        		'maintenance'=>0
        	],
        	[
        		'module'=>'Job Order',
        		'submodule'=>'Job Order',
        		'action'=>'Upload',
        		'operation'=>1,
        		'maintenance'=>0
        	],
        	[
        		'module'=>'Job Order',
        		'submodule'=>'Job Order',
        		'action'=>'Preview & Download PDF',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Field Activity',
        		'submodule'=>'Field Activity',
        		'action'=>'Search',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Field Activity',
        		'submodule'=>'Field Activity',
        		'action'=>'Create',
        		'operation'=>0,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Field Activity',
        		'submodule'=>'Field Activity',
        		'action'=>'Read',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Field Activity',
        		'submodule'=>'Field Activity',
        		'action'=>'Update',
        		'operation'=>0,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Field Activity',
        		'submodule'=>'Field Activity',
        		'action'=>'Delete',
        		'operation'=>0,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Field Activity',
        		'submodule'=>'Field Activity',
        		'action'=>'Upload',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
        		'module'=>'Field Activity',
        		'submodule'=>'Field Activity',
        		'action'=>'Preview & Download PDF',
        		'operation'=>1,
        		'maintenance'=>1
        	],
        	[
                'module'=>'Setup',
                'submodule'=>'Project',
                'action'=>'View',
                'operation'=>0,
                'maintenance'=>1
            ],
            [
                'module'=>'Setup',
                'submodule'=>'User Access',
                'action'=>'View',
                'operation'=>0,
                'maintenance'=>1
            ],
            [
        		'module'=>'Setup',
        		'submodule'=>'Audit Trail',
        		'action'=>'View',
        		'operation'=>0,
        		'maintenance'=>1
        	]
        ]);
    }
}
