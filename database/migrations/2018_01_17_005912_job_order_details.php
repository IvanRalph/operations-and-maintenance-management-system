<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jo_no');
            $table->integer('ir_no')->unsigned();
            $table->string('priority_level');
            $table->dateTime('schedule')->nullable();
            $table->longText('asset_description');
            $table->longText('brief_description');
            $table->longText('details');
            $table->longText('remarks');
            $table->integer('issued_by')->unsigned();
            $table->integer('isDeleted')->default(0);
            $table->timestamps();

            $table->foreign('ir_no')
            ->references('id')->on('incident_details')
            ->onUpdate('cascade');

            $table->foreign('issued_by')
            ->references('id')->on('tjsg_hris.employee_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_details');
    }
}
