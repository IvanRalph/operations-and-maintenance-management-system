<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FieldActivityDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_activity_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('far_no');
            $table->integer('jo_no')->unsigned();
            $table->longText('issue_description');
            $table->longText('observation');
            $table->longText('actions');
            $table->longText('remarks')->nullable();
            $table->integer('isDeleted')->default(0);
            $table->integer('in_charge')->unsigned();
            $table->timestamps();

            $table->foreign('jo_no')
            ->references('id')->on('job_order_details')
            ->onUpdate('cascade');

            $table->foreign('in_charge')
            ->references('id')->on('tjsg_hris.employee_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_activity_details');
    }
}
