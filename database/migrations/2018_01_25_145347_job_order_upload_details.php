<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobOrderUploadDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_upload_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jo_no')->unsigned();
            $table->string('filename');
            $table->integer('deleted')->default(0);
            $table->timestamps();

            $table->foreign('jo_no')
            ->references('id')->on('job_order_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_upload_details');
    }
}
