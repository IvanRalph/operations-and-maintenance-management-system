<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAccessDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_access_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->unsigned();
            $table->integer('user_type_id')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->timestamps();

            $table->foreign('employee_id')
            ->references('id')->on('tjsg_hris.employee_details')
            ->onUpdate('cascade');

            $table->foreign('user_type_id')
            ->references('id')->on('user_types')
            ->onUpdate('cascade');

            $table->foreign('updated_by')
            ->references('id')->on('tjsg_hris.employee_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_access_details');
    }
}
