<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncidentUploadDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_upload_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ir_no')->unsigned();
            $table->string('filename');
            $table->integer('deleted')->default(0);
            $table->timestamps();

            $table->foreign('ir_no')
            ->references('id')->on('incident_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident_upload_details');
    }
}
