<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessMatrixSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_matrix_systems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module');
            $table->string('submodule');
            $table->string('action');
            $table->integer('operation');
            $table->integer('maintenance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_matrix_systems');
    }
}
