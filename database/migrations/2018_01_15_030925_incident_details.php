<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncidentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ir_no');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')
            ->references('id')->on('project_details')
            ->onUpdate('cascade');
            $table->integer('prepared_by')->unsigned();
            $table->foreign('prepared_by')
            ->references('id')->on('tjsg_hris.employee_details')
            ->onUpdate('cascade');
            $table->longText('brief_description');
            $table->longText('incident_details');
            $table->longText('other_comments');
            $table->string('status');
            $table->longText('status_remarks')->nullable();
            $table->dateTime('status_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident_details');
    }
}
