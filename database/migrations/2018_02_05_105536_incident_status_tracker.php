<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncidentStatusTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_status_tracker', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ir_no')->unsigned();
            $table->string('status');
            $table->longText('remarks')->nullable();
            $table->integer('updated_by')->unsigned();
            $table->timestamps();

            $table->foreign('updated_by')
            ->references('id')->on('tjsg_hris.employee_details')
            ->onUpdate('cascade');

            $table->foreign('ir_no')
            ->references('id')->on('incident_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident_status_tracker');
    }
}
