<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FieldActivityUploadDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_activity_upload_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('far_no')->unsigned();
            $table->string('filename');
            $table->integer('deleted')->default(0);
            $table->timestamps();

            $table->foreign('far_no')
            ->references('id')->on('field_activity_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_activity_upload_details');
    }
}
