<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_name');
            $table->string('plant_id');
            $table->string('plant_type');
            $table->string('dc_capacity');
            $table->string('ac_capacity');
            $table->longText('location');
            $table->date('date_commissioned');
            $table->string('pv_modules');
            $table->string('inverter');
            $table->string('communication');
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('created_by')
            ->references('id')->on('tjsg_hris.employee_details')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_details');
    }
}
