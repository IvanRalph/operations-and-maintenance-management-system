<!DOCTYPE html>
<html>
<head>
	<title>{{ $incident->ir_no or 'IR-000X' }}</title>
	<link rel="icon" type="image/png" href="http://192.168.0.226:81/img/solar-logo-xs.png">
	<style type="text/css">
		.page-break {
			page-break-after: always;
		}

		body, p {
			font-size: 75%!important;
		}

		@page { margin: 0 20px; }

		.table td{
		    border: #333 solid 1px !important;
		}

		:root {
		  --blue: #007bff;
		  --indigo: #6610f2;
		  --purple: #6f42c1;
		  --pink: #e83e8c;
		  --red: #dc3545;
		  --orange: #fd7e14;
		  --yellow: #ffc107;
		  --green: #28a745;
		  --teal: #20c997;
		  --cyan: #17a2b8;
		  --white: #fff;
		  --gray: #6c757d;
		  --gray-dark: #343a40;
		  --primary: #007bff;
		  --secondary: #6c757d;
		  --success: #28a745;
		  --info: #17a2b8;
		  --warning: #ffc107;
		  --danger: #dc3545;
		  --light: #f8f9fa;
		  --dark: #343a40;
		  --breakpoint-xs: 0;
		  --breakpoint-sm: 576px;
		  --breakpoint-md: 768px;
		  --breakpoint-lg: 992px;
		  --breakpoint-xl: 1200px;
		  --font-family-sans-serif: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
		  --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
		}

		*,
		*::before,
		*::after {
		  box-sizing: border-box;
		}

		html {
		  font-family: sans-serif;
		  line-height: 1.15;
		  -webkit-text-size-adjust: 100%;
		  -ms-text-size-adjust: 100%;
		  -ms-overflow-style: scrollbar;
		  -webkit-tap-highlight-color: transparent;
		}

		@-ms-viewport {
		  width: device-width;
		}

		body {
		  margin: 0;
		  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
		  font-size: 1rem;
		  font-weight: 400;
		  line-height: 1.5;
		  color: #212529;
		  text-align: left;
		  background-color: #fff;
		}

		p {
		  margin-top: 0;
		  margin-bottom: 1rem;
		}

		b {
		  font-weight: bolder;
		}

		img {
		  vertical-align: middle;
		  border-style: none;
		}

		table {
		  border-collapse: collapse;
		}

		::-webkit-file-upload-button {
		  font: inherit;
		  -webkit-appearance: button;
		}

		.row {
		  display: -webkit-box;
		  display: -ms-flexbox;
		  display: flex;
		  -ms-flex-wrap: wrap;
		  flex-wrap: wrap;
		  margin-right: -15px;
		  margin-left: -15px;
		}

		.table {
		  width: 100%;
		  max-width: 100%;
		  margin-bottom: 1rem;
		  background-color: transparent;
		}

		.table td {
		  padding: 0.75rem;
		  vertical-align: top;
		  border-top: 1px solid #dee2e6;
		}

		.table-sm td {
		  padding: 0.3rem;
		}

		.table-bordered {
		  border: 1px solid #dee2e6;
		}

		.table-bordered td {
		  border: 1px solid #dee2e6;
		}

		.btn:not(:disabled):not(.disabled) {
		  cursor: pointer;
		}

		.btn:not(:disabled):not(.disabled):active, .btn:not(:disabled):not(.disabled).active {
		  background-image: none;
		}

		.btn-primary:not(:disabled):not(.disabled):active, .btn-primary:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #0062cc;
		  border-color: #005cbf;
		}

		.btn-primary:not(:disabled):not(.disabled):active:focus, .btn-primary:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.5);
		}

		.btn-secondary:not(:disabled):not(.disabled):active, .btn-secondary:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #545b62;
		  border-color: #4e555b;
		}

		.btn-secondary:not(:disabled):not(.disabled):active:focus, .btn-secondary:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(108, 117, 125, 0.5);
		}

		.btn-success:not(:disabled):not(.disabled):active, .btn-success:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #1e7e34;
		  border-color: #1c7430;
		}

		.btn-success:not(:disabled):not(.disabled):active:focus, .btn-success:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(40, 167, 69, 0.5);
		}

		.btn-info:not(:disabled):not(.disabled):active, .btn-info:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #117a8b;
		  border-color: #10707f;
		}

		.btn-info:not(:disabled):not(.disabled):active:focus, .btn-info:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(23, 162, 184, 0.5);
		}

		.btn-warning:not(:disabled):not(.disabled):active, .btn-warning:not(:disabled):not(.disabled).active {
		  color: #212529;
		  background-color: #d39e00;
		  border-color: #c69500;
		}

		.btn-warning:not(:disabled):not(.disabled):active:focus, .btn-warning:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(255, 193, 7, 0.5);
		}

		.btn-danger:not(:disabled):not(.disabled):active, .btn-danger:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #bd2130;
		  border-color: #b21f2d;
		}

		.btn-danger:not(:disabled):not(.disabled):active:focus, .btn-danger:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.5);
		}

		.btn-light:not(:disabled):not(.disabled):active, .btn-light:not(:disabled):not(.disabled).active {
		  color: #212529;
		  background-color: #dae0e5;
		  border-color: #d3d9df;
		}

		.btn-light:not(:disabled):not(.disabled):active:focus, .btn-light:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(248, 249, 250, 0.5);
		}

		.btn-dark:not(:disabled):not(.disabled):active, .btn-dark:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #1d2124;
		  border-color: #171a1d;
		}

		.btn-dark:not(:disabled):not(.disabled):active:focus, .btn-dark:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(52, 58, 64, 0.5);
		}

		.btn-outline-primary:not(:disabled):not(.disabled):active, .btn-outline-primary:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #007bff;
		  border-color: #007bff;
		}

		.btn-outline-primary:not(:disabled):not(.disabled):active:focus, .btn-outline-primary:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.5);
		}

		.btn-outline-secondary:not(:disabled):not(.disabled):active, .btn-outline-secondary:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #6c757d;
		  border-color: #6c757d;
		}

		.btn-outline-secondary:not(:disabled):not(.disabled):active:focus, .btn-outline-secondary:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(108, 117, 125, 0.5);
		}

		.btn-outline-success:not(:disabled):not(.disabled):active, .btn-outline-success:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #28a745;
		  border-color: #28a745;
		}

		.btn-outline-success:not(:disabled):not(.disabled):active:focus, .btn-outline-success:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(40, 167, 69, 0.5);
		}

		.btn-outline-info:not(:disabled):not(.disabled):active, .btn-outline-info:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #17a2b8;
		  border-color: #17a2b8;
		}

		.btn-outline-info:not(:disabled):not(.disabled):active:focus, .btn-outline-info:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(23, 162, 184, 0.5);
		}

		.btn-outline-warning:not(:disabled):not(.disabled):active, .btn-outline-warning:not(:disabled):not(.disabled).active {
		  color: #212529;
		  background-color: #ffc107;
		  border-color: #ffc107;
		}

		.btn-outline-warning:not(:disabled):not(.disabled):active:focus, .btn-outline-warning:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(255, 193, 7, 0.5);
		}

		.btn-outline-danger:not(:disabled):not(.disabled):active, .btn-outline-danger:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #dc3545;
		  border-color: #dc3545;
		}

		.btn-outline-danger:not(:disabled):not(.disabled):active:focus, .btn-outline-danger:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.5);
		}

		.btn-outline-light:not(:disabled):not(.disabled):active, .btn-outline-light:not(:disabled):not(.disabled).active {
		  color: #212529;
		  background-color: #f8f9fa;
		  border-color: #f8f9fa;
		}

		.btn-outline-light:not(:disabled):not(.disabled):active:focus, .btn-outline-light:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(248, 249, 250, 0.5);
		}

		.btn-outline-dark:not(:disabled):not(.disabled):active, .btn-outline-dark:not(:disabled):not(.disabled).active {
		  color: #fff;
		  background-color: #343a40;
		  border-color: #343a40;
		}

		.btn-outline-dark:not(:disabled):not(.disabled):active:focus, .btn-outline-dark:not(:disabled):not(.disabled).active:focus {
		  box-shadow: 0 0 0 0.2rem rgba(52, 58, 64, 0.5);
		}

		.navbar-toggler:not(:disabled):not(.disabled) {
		  cursor: pointer;
		}

		.page-link:not(:disabled):not(.disabled) {
		  cursor: pointer;
		}

		.close:not(:disabled):not(.disabled) {
		  cursor: pointer;
		}

		@supports ((-webkit-transform-style: preserve-3d) or (transform-style: preserve-3d)) {
		}

		@supports ((-webkit-transform-style: preserve-3d) or (transform-style: preserve-3d)) {
		}

		@supports ((-webkit-transform-style: preserve-3d) or (transform-style: preserve-3d)) {
		}

		.align-top {
		  vertical-align: top !important;
		}

		.align-middle {
		  vertical-align: middle !important;
		}

		@supports ((position: -webkit-sticky) or (position: sticky)) {
		}

		.text-left {
		  text-align: left !important;
		}

		.text-right {
		  text-align: right !important;
		}

		.text-center {
		  text-align: center !important;
		}

		@media print {
		  *,
		  *::before,
		  *::after {
		    text-shadow: none !important;
		    box-shadow: none !important;
		  }
		  tr,
		  img {
		    page-break-inside: avoid;
		  }
		  p {
		    orphans: 3;
		    widows: 3;
		  }
		  @page {
		    size: a3;
		  }
		  body {
		    min-width: 992px !important;
		  }
		  .table {
		    border-collapse: collapse !important;
		  }
		  .table td {
		    background-color: #fff !important;
		  }
		  .table-bordered td {
		    border: 1px solid #ddd !important;
		  }
		}
		/*# sourceMappingURL=bootstrap.css.map */
	</style>
</head>
<body>
		<table class="table table-bordered table-sm text-center" style="margin-bottom: 0;">
		  <tbody>
		    <tr>
		    <td rowspan="3" style="width: 25%;" class="align-middle"><img src="http://192.168.0.226:81/img/solar-logo-sm.png" width="150px"></td>
		    <td class="text-center" rowspan="3" colspan="2" style="width: 50%;"><br><b style="font-size: 12px;">SOLAR PHILIPPINES COMMERCIAL ROOFTOP PROJECTS INC.</b><p style="font-size: 10px;">LPL Towers, 112 Legaspi St. Legaspi Village, Makati City</p><br><b style="font-size: 15px!important;">INCIDENT REPORT</b></td>
		    <td class="align-middle" style="width: 12.5%;">Doc No.:</td>
		    <td class="align-middle" style="width: 12.5%;">FAR</td>
	    </tr>
	    <tr>
	    	<td class="align-middle">Effective Date:</td>
	    	<td class="align-middle">09/14/2017</td>
	    </tr>
	    <tr>
	    	<td colspan="2"></td>
	    </tr>
		    <tr>
		    	<td colspan="5" rowspan="3" class="text-right align-top"><br>IR NO.: <u>{{ $incident->ir_no or 'IR-000X' }}<span style="color: #fff;">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;</span></u><br>DATE: <u>{!! date_format($incident->created_at, 'F j, Y') !!}<span style="color: #fff;">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span></u></td>
		    </tr>
		    <tr>
		    	{{-- FOR IR NO AND DATE --}}
		    </tr>
		    <tr>
		    	{{-- FOR IR NO AND DATE --}}
		    </tr>
		    <tr>
		    	<td colspan="5" style="background-color: #333; color: #fff;">GENERAL DETAILS</td>
		    </tr>
		    <tr class="text-left">
		    	<td>
					<b>PROJECT:</b>		    		
		    	</td>
		    	<td style="font-size: 12px;">{{ $incident->project_name or 'Lorem ispum dolor sit amet' }}</td>
		    	<td><b>PLANT ID:</b></td>
		    	<td colspan="2" style="font-size: 12px;">{{ $incident->plant_id or 'PID-000X'}}</td>
		    </tr>
	        <tr class="text-left">
	        	<td>
	    			<b>LOCATION:</b>		    		
	        	</td>
	        	<td style="font-size: 12px;">{{ $incident->location or 'Lorem ispum dolor sit amet'}}</td>
	        	<td><b>CAPACITY:</b></td>
	        	<td colspan="2" style="font-size: 12px;">{{ $incident->dc_capacity or '' }}</td>
	        </tr>
	        <tr>
	        	<td colspan="5" style="color: #fff;">&emsp;</td>
	        </tr>
	        <tr>
	        	<td colspan="5" style="background-color: #333; color: #fff;">INCIDENT DETAILS</td>
	        </tr>
	        <tr class="text-left">
	        	<td><b>BRIEF DESCRIPTION:</b></td>
	        	<td colspan="4">{{ $incident->brief_description or 'Lorem ipsum dolor sit amet'}}</td>
	        </tr>
	        <tr class="text-left">
	        	<td colspan="5" rowspan="14"><b>INCIDENT DETAILS:</b><p style="font-size: 12px;">{{ $incident->incident_details or '' }}</p></td>
	        </tr>
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- THIS ARE ALL FOR INCIDENT DETAILS --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr></tr> {{-- -- --}}
	        <tr class="text-left">
	        	<td colspan="5" rowspan="5"><b>OTHER COMMENTS:</b> <p style="font-size: 12px;">{{ $incident->other_comments or '' }}</p></td>
	        </tr>
	        <tr></tr>
	        <tr></tr>
	        <tr></tr>
	        <tr></tr>
	        <tr>
	        	<td colspan="5" style="color: #fff;">&emsp;</td>
	        </tr>
		  </tbody>
		</table>
		<table class="table table-bordered table-sm text-center">
			<tbody>
				<tr>
					<td colspan="3" style="background-color: #333; color: #333;">?</td>
				</tr>
				<tr class="text-center">
					<td style="width: 33.33%;"><b>PREPARED BY:</b></td>
					<td style="width: 33.33%;"><b>CONFORMED BY:</b></td>
					<td style="width: 33.33%;"><b>NOTED BY:</b></td>
				</tr>
				<tr class="text-center">
					<td rowspan="3"><div style="font-size: 10px;"><br><br><b style="font-size: 12px!important;">{{ $incident->prepared_by }}</b><br>Signature over Printed Name</div><u style="font-size: 12px;">{!! date_format($incident->created_at, 'F j, Y') !!}</u><br><span style="font-size: 10px;">DATE</span></td>
					<td rowspan="3"><div style="font-size: 10px;"><br><br><br>Signature over Printed Name</div><u style="font-size: 12px;"></u><br><span style="font-size: 10px;">DATE</span></td>
					<td rowspan="3"><div style="font-size: 10px;"><br><br><br>Signature over Printed Name</div><u style="font-size: 12px;"></u><br><span style="font-size: 10px;">DATE</span></td>
				</tr>
				<tr></tr>
				<tr></tr>
			</tbody>
		</table>
</body>
</html>