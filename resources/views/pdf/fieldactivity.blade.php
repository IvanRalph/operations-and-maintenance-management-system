<!DOCTYPE html>
<html>
<head>
	<title>{{ $fieldActivity->far_no or 'FAR-000X' }}</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="icon" type="image/png" href="/img/solar-logo-xs.png">
	<style type="text/css">
		.page-break {
		    page-break-after: always;
		}

		body, p {
			font-size: 75%!important;
		}
	</style>
</head>
<body>
	<table class="table table-bordered table-sm text-center" style="margin-bottom: 0;">
	  <tbody>
	  	<tr>
		    <td rowspan="3" style="width: 25%;" class="align-middle"><img src="http://192.168.0.226:81/img/solar-logo-sm.png" width="150px"></td>
		    <td class="text-center" rowspan="3" colspan="2" style="width: 50%;"><br><b style="font-size: 12px;">SOLAR PHILIPPINES COMMERCIAL ROOFTOP PROJECTS INC.</b><p style="font-size: 10px;">LPL Towers, 112 Legaspi St. Legaspi Village, Makati City</p><br><b style="font-size: 15px!important;">FIELD ACTIVITY REPORT</b></td>
		    <td class="align-middle" style="width: 12.5%;">Doc No.:</td>
		    <td class="align-middle" style="width: 12.5%;">FAR</td>
	    </tr>
	    <tr>
	    	<td class="align-middle">Effective Date:</td>
	    	<td class="align-middle">09/14/2017</td>
	    </tr>
	    <tr>
	    	<td colspan="2"></td>
	    </tr>
	    <tr>
	    	<td colspan="5" rowspan="3" class="text-right align-top"><br>FAR NO.: <u><span style="color: #e55039;">{{ $fieldActivity->far_no or 'FAR-000X' }}</span><span style="color: #fff;">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;</span></u><br>DATE: <u><span style="color: #e55039;">{!! date_format($fieldActivity->created_at, 'F j, Y') !!}</span><span style="color: #fff;">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span></u></td>
	    </tr>
	    <tr>
	    	{{-- FOR IR NO AND DATE --}}
	    </tr>
	    <tr>
	    	{{-- FOR IR NO AND DATE --}}
	    </tr>
	    <tr>
	    	<td colspan="5" style="background-color: #333; color: #fff;">GENERAL DETAILS</td>
	    </tr>
	    <tr class="text-left">
	    	<td>
				<b>PROJECT:</b>		    		
	    	</td>
	    	<td style="font-size: 12px;">{{ $fieldActivity->project_name or 'Lorem ispum dolor sit amet' }}</td>
	    	<td><b>CAPACITY:</b></td>
	    	<td colspan="2" style="font-size: 12px;">{{ $fieldActivity->dc_capacity or 'PID-000X'}}</td>
	    </tr>
        <tr class="text-left">
        	<td>
    			<b>LOCATION:</b>		    		
        	</td>
        	<td style="font-size: 12px;">{{ $fieldActivity->location or 'Lorem ispum dolor sit amet'}}</td>
        	<td><b>REFERENCE JO:</b></td>
        	<td colspan="2" style="font-size: 12px;">{{ $fieldActivity->jo_no or '' }}</td>
        </tr>
        <tr>
        	<td colspan="5" style="color: #fff;">&emsp;</td>
        </tr>
        <tr>
        	<td colspan="5" style="background-color: #333; color: #fff;">DETAILS OF ACTIVITY</td>
        </tr>
        <tr class="text-left">
        	<td>
				<b>ASSET DESCRIPTION:</b>		    		
	    	</td>
	    	<td style="font-size: 12px;" colspan="4">{{ $fieldActivity->asset_description or 'Lorem ispum dolor sit amet' }}</td>
        </tr>
        <tr class="text-left">
        	<td rowspan="5" colspan="5"><b>ISSUE/BRIEF DESCRIPTION:</b><p style="font-size: 12px;">{!! $fieldActivity->issue_description or '<span style="color:#fff;">&emsp;</span>' !!}</p></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr class="text-left">
        	<td colspan="5" rowspan="5"><b>OBSERVATION:</b><p style="font-size: 12px;">{!! $fieldActivity->observation or '<span style="color:#fff;">&emsp;</span>' !!}</p></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr class="text-left">
        	<td colspan="5" rowspan="5"><b>ACTIONS:</b><p style="font-size: 12px;">{!! $fieldActivity->actions or '<span style="color:#fff;">&emsp;</span>' !!}</p></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr class="text-left">
        	<td colspan="5" rowspan="5"><b>REMARKS:</b> <p style="font-size: 12px;">{!! $fieldActivity->remarks or '<span style="color:#fff;">&emsp;</span>' !!}</p></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr>
        	<td colspan="5" style="color: #fff;">&emsp;</td>
        </tr>
        <tr>
	    	<td colspan="5" style="background-color: #333; color: #fff;">MANPOWER</td>
	    </tr>
	    <tr>
	    	<td><b>NAME</b></td>
	    	<td><b>SIGNATURE</b></td>
	    	<td><b>NAME</b></td>
	    	<td colspan="2"><b>SIGNATURE</b></td>
	    </tr>
	    <tr style="color: #fff;">
	    	<td>&emsp;</td>
	    	<td>&emsp;</td>
	    	<td>&emsp;</td>
	    	<td colspan="2">&emsp;</td>
	    </tr>
	    <tr style="color: #fff;">
	    	<td>&emsp;</td>
	    	<td>&emsp;</td>
	    	<td>&emsp;</td>
	    	<td colspan="2">&emsp;</td>
	    </tr>
	    <tr style="color: #fff;">
	    	<td>&emsp;</td>
	    	<td>&emsp;</td>
	    	<td>&emsp;</td>
	    	<td colspan="2">&emsp;</td>
	    </tr>
	  </tbody>
	</table>
	<table class="table table-bordered table-sm text-center">
		<tbody>
			<tr>
				<td colspan="3" style="background-color: #333; color: #fff;">ACKNOWLEDGEMENT AND APPROVAL</td>
			</tr>
			<tr class="text-center">
				<td style="width: 33.33%;"><b>IN-CHARGE:</b></td>
				<td style="width: 33.33%;"><b>ACKNOWLEDGED BY:</b></td>
				<td style="width: 33.33%;"><b>CONFORMED BY:</b></td>
			</tr>
			<tr class="text-center">
				<td rowspan="3"><div style="font-size: 10px;"><br><br>Signature over Printed Name</div><u style="font-size: 12px;"></u><br><span style="font-size: 10px;">DATE</span></td>
				<td rowspan="3"><div style="font-size: 10px;"><br><br>Signature over Printed Name</div><u style="font-size: 12px;"></u><br><span style="font-size: 10px;">DATE</span></td>
				<td rowspan="3"><div style="font-size: 10px;"><br><br>Signature over Printed Name</div><u style="font-size: 12px;"></u><br><span style="font-size: 10px;">DATE</span></td>
			</tr>
			<tr></tr>
			<tr></tr>
		</tbody>
	</table>
</body>
</html>