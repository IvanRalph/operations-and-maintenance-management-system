@extends('errors::layout')

@section('title', $exception->getMessage())

@section('message', $exception->getMessage())
