@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Dashboard</h1>
<ol class="breadcrumb">
	<li><i class="fa fa-dashboard"></i> Dashboard</li>
</ol>
@stop

@section('content')
<div class="row">
	<div class="col-sm-10 col-sm-offset-3">
		<div class="col-sm-4 ">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-folder-open-o"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">Open</span>
					<span class="info-box-number">{{ $incident->open }}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>

		<div class="col-sm-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-refresh"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">In Progress</span>
					<span class="info-box-number">{{ $incident->in_progress }}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
		<div class="col-sm-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-recycle"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">Reopened</span>
					<span class="info-box-number">{{ $incident->reopen }}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>

		<div class="col-sm-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-times-circle-o"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">Closed</span>
					<span class="info-box-number">{{ $incident->closed }}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>

		<div class="col-sm-4">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-exchange"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">Unresolved</span>
					<span class="info-box-number">{{ $incident->unresolved }}</span>
				</div>
				<!-- /.info-box-content -->
			</div>
		</div>
	</div>
</div>

<h3>Quick Links</h3>
<div class="row">
	@can('access-matrix', 3)  
	<div class="col-sm-4">
		<a href="/incidents/create" class="btn btn-default btn-block">Create Incident Report</a>
	</div>
	@endcan
	

	@can('access-matrix', 10) 
	<div class="col-sm-4">
		<a href="/job-orders/create" class="btn btn-default btn-block">Create Job Order</a>
	</div>
	@endcan

	@can('access-matrix', 17)  
	<div class="col-sm-4">
		<a href="/field-activities/create" class="btn btn-default btn-block">Create Field Activity Report</a>
	</div>
	@endcan
</div>
@stop