@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Setup > Audit Trail</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Setup</li>
	<li>Audit Trail</li>
</ol>
@stop

@section('content')
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>Module</th>
			<th>Sub Module</th>
			<th>Action</th>
			<th>Create Date</th>
			<th>Created By</th>
			<th>Updated Date</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/auditTrailData',
		columns: [
		{ data: 'module', width: '100px' },
		{ data: 'submodule', width: '100px' },
		{ data: 'action', width: '100%' },
		{ data: 'created_at', width: '75px' },
		{ data: 'created_by', width: '75px' },
		{ data: 'updated_at', width: '75px' },
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5]
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});
</script>
@stop