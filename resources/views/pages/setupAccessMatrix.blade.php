@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Setup > Access Matrix</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Setup</li> 
	<li>Access Matrix</li>
</ol>
@stop

@section('content')
<form action="{{ action('AccessMatrixController@store') }}" method="POST">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
				<thead>
					<tr>
						<th>Module</th>
						<th>Sub Module</th>
						<th>Action</th>
						<th>Operation</th>
						<th>Maintenance</th>
					</tr>
				</thead>
				<tbody>
					@for ($i = 0; $i < $count; $i++)
					<input type="hidden" name="accessId" value="{{ $acl[$i]->id }}">
						<tr class="@if($i == 0) active @elseif($i <= 7) success @elseif($i <= 14) info @elseif($i <= 21) warning @else danger @endif">
							<td>{{ $acl[$i]->module }}</td>
							<td>{{ $acl[$i]->submodule }}</td>
							<td title="{{ $i+1 }}">{{ $acl[$i]->action }}</td>
							<td><input type="checkbox" data-id="{{ $acl[$i]->id }}" {{ $acl[$i]->operation == 1 ? 'checked' : ''}} name="op_{{ $acl[$i]->id }}"></td>
							<td><input type="checkbox" data-id="{{ $acl[$i]->id }}" {{ $acl[$i]->maintenance == 1 ? 'checked' : ''}} name="main_{{ $acl[$i]->id }}"></td>
						</tr>
					@endfor
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="row col-md-offset-2">
		<div class="col-md-4 col-sm-12">
			<input type="submit" name="submit" data-loading="Loading..." class="btn btn-primary btn-block" value="SAVE">
		</div>
		<div class="col-md-4 col-sm-12">
			<a href="/" class="btn btn-default form-control">CANCEL</a>
		</div>
	</div>
</form>
@stop

@section('js')
<script type="text/javascript">
	$('input[name="submit"]').on('click', function(){
		$(this).button('loading');
	})

	$(document).on('click', '#mainTable td', function(){
		if($(this).find('input').attr('checked')){
			$(this).find('input').removeAttr('checked');
		}else{
			$(this).find('input').attr('checked', 'checked');
		}
	});
</script>
@stop