@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Setup > Project</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Setup </li>
	<li>Project</li>
</ol>
@stop

@section('content')
<div class='notifications top-right'></div>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>Project Name</th>
			<th>Plant ID</th>
			<th>Plant Type</th>
			<th>DC Capacity</th>
			<th>AC Capacity</th>
			<th>Location</th>
			<th>Date Commisioned</th>
			<th>PV Modules</th>
			<th>Inverter</th>
			<th>Communication</th>
			<th>Created Date</th>
			<th>Created By</th>
			<th>Updated Date</th>
			<th>Updated By</th>
			<th>Action Items</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
	});
	@isset($projectCreated)
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "A project has been created." }
		}).show();
	});
	@endisset

	@isset($projectUpdated)
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "A project has been updated." }
		}).show();
	});
	@endisset
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/projectData',
		columns: [
		{ data: 'project_name', width: '150px' },
		{ data: 'plant_id' },
		{ data: 'plant_type', visible: false },
		{ data: 'dc_capacity' },
		{ data: 'ac_capacity' },
		{ data: 'location', width: '150px' },
		{ data: 'date_commissioned' },
		{ data: 'pv_modules' },
		{ data: 'inverter' },
		{ data: 'communication' },
		{ data: 'created_at', width: '100px' },
		{ data: 'created_by', width: '100px' },
		{ data: 'updated_at', width: '100px' },
		{ data: 'updated_by', width: '100px' },
		{ data: null, width: '200px', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn = "<a href='/projects/"+ data.id +"' class='btn btn-default'>View</a>";
				btn += "<a href='/projects/"+data.id+"/edit' class='btn btn-default'>Update</a>";
				if(data.created_by == '{{ \Auth::user()->getDetails()->firstname . ' ' . \Auth::user()->getDetails()->lastname }}'){
					btn += "<a href='' data-id='"+ data.id +"' class='btn btn-default deleteBtn'>Delete</a>";
				}

				return btn;
			}
		},
		{
			targets: [0,5],
			render: function(data, type, row){
				if(data != null){
					return data.length > 20 ?
					'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 20 ) + '...' + '</span>':
					data;
				}else{
					return data;
				}
			}
		}
		],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
			}
		},
		{
			text: 'Create Project',
			action: function ( e, dt, node, config ) {
				window.location.href="/projects/create";
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});

	$('#mainTable').on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
	
    $(document).on('click', '.deleteBtn', function(e){
    	e.preventDefault();
    	swal({
    		text: 'Are you sure you want to delete this project?',
    		showCancelButton: true,
    		cancelButtonText: 'No',
    		confirmButtonText: 'Yes',
    		type: 'question'
    	}).then(function(){
    		$.ajax({
    			url: '../projects/' + $('.deleteBtn').data('id'),
    			type: 'DELETE',
    			dataType: 'JSON',
    			success: function(data, result){
    				swal(data.title, data.msg, data.type).then(function(){
    					table.ajax.reload();
    				});
    			}
    		});
    	});
    })
</script>
@stop