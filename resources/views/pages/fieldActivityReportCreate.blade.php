@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Field Activity Report > @if(isset($showFieldActivity))View Field Activity @elseif(isset($updateFieldActivity))Update Field Activity @else Create Field Activity @endif</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="/field-activities"><i class="fa fa-line-chart"></i> Field Activities</a></li>
	<li> Create Field Activity Report</li>
</ol>
@stop

@section('content')
<div class='notifications top-right'></div>
<!-- form start -->
<form action="{{ isset($updateFieldActivity) ? action('FieldActivityController@update', $updateFieldActivity->id) : action('FieldActivityController@store') }}" method="POST" enctype="multipart/form-data">
	@if(!isset($showFieldActivity))
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	@endif
	@if(isset($updateFieldActivity))
	<input type="hidden" name="_method" value="patch">
	@endif
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Job Order Details</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('jo_no') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">JO No.</label>
						<div class="col-sm-7">
							@isset($showFieldActivity)
							<input type="text" class="form-control" name="" disabled value="{{ $showFieldActivity->jo_no }}">
							@else
							<select id="jo_no" name="jo_no" class="form-control"></select>
							@endisset
							@if($errors->has('jo_no'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('jo_no') }}</span>
							@endif
						</div>
						<div class="col-sm-3">
							<input type="button" class="form-control btn btn-default" data-toggle="modal" data-target=".previewJoModal" name="preview_jo" disabled value="Preview JO">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Project</label>
						<div class="col-sm-10">
							<input type="text" name="project" class="form-control" disabled value="{{ isset($showFieldActivity) ? $showFieldActivity->project_name : '' }}">
						</div>
					</div>
				</div>
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Plant ID</label>
						<div class="col-sm-10">
							<input type="text" name="plant_id" class="form-control" disabled value="{{ isset($showFieldActivity) ? $showFieldActivity->plant_id : '' }}">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Location</label>
						<div class="col-sm-10">
							<input type="text" name="location" class="form-control" disabled value="{{ isset($showFieldActivity) ? $showFieldActivity->location : '' }}">
						</div>
					</div>
				</div>
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">DC Capacity</label>
						<div class="col-sm-10">
							<input type="text" name="location" class="form-control" disabled value="{{ isset($showFieldActivity) ? $showFieldActivity->dc_capacity : '' }}">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Asset Description</label>
						<div class="col-sm-10">
							<input type="text" name="asset_description" class="form-control" disabled value="{{ isset($showFieldActivity) ? $showFieldActivity->asset_description : '' }}">
						</div>
					</div>
				</div>
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Priority Level</label>
						<div class="col-sm-10">
							<input type="text" name="priority_level" class="form-control" disabled value="{{ isset($showFieldActivity) ? $showFieldActivity->priority_level : '' }}">
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Activity Details</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="row">
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">FAR No.</label>
						<div class="col-sm-10">
							<input type="text" name="far_no" class="form-control" disabled value="{{ isset($showFieldActivity) ? $showFieldActivity->project_name : '' }}{{ isset($updateFieldActivity) ? $updateFieldActivity->far_no : '' }}{{ !isset($showFieldActivity) && !isset($updateFieldActivity) ? 'FAR-000X' : '' }}">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('issue_description') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Issue Description</label>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<textarea name="issue_description" class="form-control" {{ isset($showFieldActivity) ? 'disabled' : '' }}>{{ old('issue_description') ? old('issue_description') : '' }}{{ isset($showFieldActivity) ? $showFieldActivity->issue_description : '' }}{{ isset($updateFieldActivity) ? $updateFieldActivity->issue_description : '' }}{{ !isset($updateFieldActivity) && old('issue_description') ? old('issue_description') : '' }}</textarea>
							@if($errors->has('issue_description'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('issue_description') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('observation') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Observation</label>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<textarea name="observation" class="form-control" {{ isset($showFieldActivity) ? 'disabled' : '' }}>{{ old('observation') ? old('observation') : '' }}{{ isset($showFieldActivity) ? $showFieldActivity->observation : '' }}{{ isset($updateFieldActivity) ? $updateFieldActivity->observation : '' }}{{ !isset($updateFieldActivity) && old('observation') ? old('observation') : '' }}</textarea>
							@if($errors->has('observation'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('observation') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('actions') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Actions</label>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<textarea name="actions" class="form-control" {{ isset($showFieldActivity) ? 'disabled' : '' }}>{{ old('actions') ? old('actions') : '' }}{{ isset($showFieldActivity) ? $showFieldActivity->actions : '' }}{{ isset($updateFieldActivity) ? $updateFieldActivity->actions : '' }}{{ !isset($updateFieldActivity) && old('actions') }}</textarea>
							@if($errors->has('actions'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('actions') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('remarks') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Remarks</label>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<textarea name="remarks" class="form-control" {{ isset($showFieldActivity) ? 'disabled' : '' }}>{{ old('remarks') ? old('remarks') : '' }}{{ isset($showFieldActivity) ? $showFieldActivity->remarks : '' }}{{ isset($updateFieldActivity) ? $updateFieldActivity->remarks : '' }}{{ !isset($updateFieldActivity) && old('remarks') ? old('remarks') : '' }}</textarea>
							@if($errors->has('remarks'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('remarks') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@can('access-matrix', 21)
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Attachments</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="form-group {{ $errors->has('attachments.*') ? 'has-error':'' }}">
				<div class="row">
					<div class="col-sm-12">
						@if((!isset($showFieldActivity) && !isset($updateFieldActivity)) || (isset($updateFieldActivity) && \Gate::allows('update-field-activity', $updateFieldActivity)))
						<input type="file" name="attachments[]" multiple>
						<p class="help-block">Accepted File Types: xlsx, xls, csv, jpg, jpeg, png, bmp, doc, docx, pdf, tif, tiff</p>
						@endif
						@if(isset($showFieldActivity) || isset($updateFieldActivity))
						@foreach($file as $f)
						<a href="{{ 'storage/'.$f->filename }}" target="_blank">{{ $f->filename }}</a> @if(isset($updateFieldActivity) && \Gate::allows('update-field-activity', $updateFieldActivity)) <a href="" class="deleteAttachment" data-id="{{ $f->id }}">  <i class="fa fa-close"></i></a> @endif&emsp;
						@endforeach
						@endif
						@if($errors->has('attachments'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('attachments') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	@endcan	

	@if(!isset($showFieldActivity))
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="col-sm-6">
				<input type="submit" name="submit" hidden>
				<button type="button" id="submitBtn" data-loading-text="Loading..." class="btn btn-primary btn-block pull-right">Submit</button>
			</div>
			<div class="col-sm-6">
				<button type="button" id="cancelBtn" class="btn btn-default btn-block pull-right">Cancel</button>
			</div>
		</div>
	</div>
	@else
		@can('access-matrix', 22)
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="col-sm-6">
					<a href="/field-activities/previewPdf/{{ $showFieldActivity->id }}" target="_blank" id="previewPdf" data-loading-text="Loading..." class="btn btn-primary btn-block pull-right">Preview PDF</a>
				</div>
				<div class="col-sm-6">
					<a href="/field-activities/downloadPdf/{{ $showFieldActivity->id }}" id="downloadPdf" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Generating PDF" class="btn btn-primary btn-block pull-right">Download PDF</a>
				</div>
			</div>
		</div>
		@endcan
	@endif
</form>

{{-- PREVIEW-MODAL --}}
<div class="modal fade previewJoModal" tabindex="-1" role="dialog" aria-labelledby="previewJo">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modalTitle"></h4>
			</div>
			<div class="modal-body">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Incident Report Details</h3>
					</div>
					<!-- /.box-header -->

					<div class="box-body">
						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">IR No.</label>
									<div class="col-sm-6">
										<input type="text" name="ir_no" class="form-control" value="" readonly>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Project</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="" name="project" readonly>
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Plant ID</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="" name="plant_id" readonly>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Location</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="" name="location" readonly>
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Capacity</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="" name="capacity" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Job Details</h3>
					</div>
					<!-- /.box-header -->

					<div class="box-body">
						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">JO No.</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="jo_no" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Schedule</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="" name="schedule" readonly>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Asset Description</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="" name="asset_description" readonly>
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Priority Level</label>
									<div class="col-sm-10">
										<input type="text" name="priority_level" class="form-control" value="" readonly>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="row">
									<label class="col-sm-3">Brief Description</label>
									<div class="col-sm-12">
										<textarea name="brief_description" class="form-control" readonly></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="row">
									<label class="col-sm-3">Details</label>
									<div class="col-sm-12">
										<textarea name="details" class="form-control" readonly></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="row">
									<label class="col-sm-3">Remarks</label>
									<div class="col-sm-12">
										<textarea name="remarks" class="form-control" readonly></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Attachments</h3>
					</div>
					<!-- /.box-header -->

					<div class="box-body">
						<div class="form-group">
							<div class="row">
								<div class="col-sm-12">
									<div class="attachments"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
@stop

@section('js')
<script>
	$(document).ready(function(){
		$('#jo_no').select2({
			allowClear: true,
			ajax: {
				url: '/autocomplete/job-order',
				dataType: 'json',
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			}
		});
	});

	$(document).on('click', '#submitBtn', function(e){
		swal({
			text: 'Are you sure you want to submit?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#submitBtn').button('loading');
			$('[type="submit"]').click();
		});
	})

	$(document).on('click', '#cancelBtn', function(){
		swal({
			text: 'Are you sure you want to cancel and discard prepared information?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/field-activities";
		});
	})

	$('#priority_level').select2({
		allowClear: true
	});

	$("#jo_no").on('change', function(){
		$.ajax({
			url: '/job-orders/' + $(this).val(),
			type: 'GET',
			data: {type: 'populateJobOrder'},
			success: function(data, status){
				$('#modalTitle').html(data.jo_no + ' - ' + data.project_name);
				$('input[name="location"]').val(data.location);
				$('input[name="capacity"]').val(data.capacity);
				$('input[name="plant_id"]').val(data.plant_id);
				$('input[name="project"]').val(data.project_name);
				$('input[name="priority_level"]').val(data.priority_level);
				$('input[name="asset_description"]').val(data.asset_description);

				$('input[name="ir_no"]').val(data.ir_no);
				$('input[name="jo_no"]').val(data.jo_no);
				$('input[name="schedule"]').val(data.schedule == "" && data.schedule.substr(0,11) || '');
				$('textarea[name="brief_description"]').val(data.brief_description);
				$('textarea[name="details"]').val(data.details);
				$('textarea[name="remarks"]:last').val(data.remarks);

				$.each(data.file, function(key,value){
					$('.attachments').append('<a href="../job-orders/storage/'+ value.filename +'">' + value.filename + '</a>&emsp;')
				});

				$('input[name="preview_jo"]').removeAttr('disabled');
			}
		});
	});

	$(document).on('click', '#downloadPdf', function(e){
		e.preventDefault();
		setTimeout(function(){
			$('#downloadPdf').button('reset');
			$('.top-right').notify({
				message: { text: "Your download will start in a few seconds, please wait.." }
			}).show();
		}, 5000);
		$(this).button('loading');
		window.location = $(this).attr('href');
	});

	@if(old('jo_no') && (!isset($showFieldActivity) && !isset($updateFieldActivity)))
	var option = new Option('{{ old('jo_no') }}', '{{ old('jo_no') }}', true, true);
	$('select[name="jo_no"]').append(option).trigger('change');
	$('select[name="jo_no"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ old('jo_no') }}'
		}
	});
	@endif

	@if(isset($updateFieldActivity))
	//retrieve jo no
	var option = new Option('{{ $updateFieldActivity->jo_no }}', '{{ $updateFieldActivity->jo_no }}', true, true);
	$('#jo_no').append(option).trigger('change');
	$('#jo_no').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateFieldActivity->ir_no }}'
		}
	});
	@endif
</script>
@stop