@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>@if(isset($showIncident))View Incident @elseif(isset($updateIncident))Update Incident @else Create Incident @endif</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="/incidents"><i class="fa fa-warning"></i> Incidents</a></li>
	<li>@if(isset($showIncident))View Incident @elseif(isset($updateIncident))Update Incident @else Create Incident @endif</li>
</ol>
@stop

@section('content')
<div class='notifications top-right'></div>
<!-- form start -->
<form id="incidentSubmit" action="{{ isset($updateIncident) ? action('IncidentController@update', $updateIncident->incident_id) : action('IncidentController@store') }}" method="POST" enctype="multipart/form-data">
	@if(!isset($showIncident))
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	@endif
	@if(isset($updateIncident))
	<input type="hidden" name="_method" value="patch">
	@endif
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">General Details</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('project') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Project</label>
						<div class="col-sm-10">
							@isset($showIncident)
							<input type="text" class="form-control" name="" value="{{ $showIncident->project_name }}" readonly>
							@else
							<select id="project" name="project" class="form-control" ></select>
							@endisset
							@if($errors->has('project'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('project') }}</span>
							@endif
						</div>
					</div>
				</div>

				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">IR No.</label>
						<div class="col-sm-6"><input type="text" name="ir_no" class="form-control" readonly value="{{ isset($showIncident) ? $showIncident->ir_no : '' }}{{ isset($updateIncident) ? $updateIncident->ir_no : '' }}{{ !isset($showIncident) && !isset($updateIncident) ? 'IR-000X' : '' }}"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('location') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-1">Location</label>
						<div class="col-sm-11">
							<input type="text" name="location" class="form-control" readonly value="{{ isset($showIncident) ? $showIncident->location : '' }}">
							@if($errors->has('location'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('location') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group row {{ $errors->has('plant_id') ? 'has-error':'' }}">
						<label class="col-sm-2">Plant ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="plant_id" readonly value="{{ isset($showIncident) ? $showIncident->plant_id : '' }}">
							@if($errors->has('plant_id'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('plant_id') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group row {{ $errors->has('dc_capacity') ? 'has-error':'' }}">
						<label class="col-sm-2">DC Capacity</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="dc_capacity" readonly value="{{ isset($showIncident) ? $showIncident->dc_capacity : '' }}{{ isset($updateIncident) ? $updateIncident->dc_capacity : '' }}{{ !isset($updateIncident) && old('dc_capacity') ? old('dc_capacity') : '' }}" {{ isset($showIncident) || (Auth::user()->isUserType() == 'Operation' && isset($updateIncident)) ? 'readonly' : '' }}>
							@if($errors->has('dc_capacity'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('dc_capacity') }}</span>
							@endif
						</div>
					</div>
					@if((isset($updateIncident) && Auth::user()->isUserType() == 'Operation') || isset($showIncident))
					<div class="form-group row {{ $errors->has('status') ? 'has-error':'' }}">
						<label class="col-sm-2">Status</label>
						<div class="col-sm-10">
							@isset($showIncident)
							<input type="text" class="form-control" name="" value="{{ $showIncident->status }}" readonly>
							@else
							<select name="status" id="status" class="form-control" title="Change this to activate status remarks" data-placement="top">
								<option></option>
								<option value="Reopen">Reopen</option>
								<option value="Closed">Closed</option>
								<option value="Unresolved">Unresolved</option>
							</select>
							@endisset
							@if($errors->has('status'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('status') }}</span>
							@endif
						</div>
					</div>
					@endif
				</div>

				@if((isset($updateIncident) && Auth::user()->isUserType() == 'Operation') || isset($showIncident))
				<div class="col-sm-6 form-group {{ $errors->has('status_remarks') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-4">Status Remarks&emsp; @isset($updateIncident)<i style="font-size: 20px;" class="fa fa-question-circle" id="remarksToolTip" data-toggle="tooltip" title="Change Status to activate this text area" data-placement="top"></i>@endisset</label>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-12">
							<textarea name="status_remarks" class="form-control" {{ isset($showIncident) ? 'readonly' : '' }}>{{ isset($showIncident) ? $showIncident->status_remarks : '' }}{{ isset($updateIncident) ? $updateIncident->status_remarks : '' }}</textarea>
							@if($errors->has('status_remarks'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('status_remarks') }}</span>
							@endif
						</div>
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Incident Details</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('brief_description') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Brief Description</label>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<textarea name="brief_description" class="form-control" {{ isset($showIncident) || (Auth::user()->isUserType() == 'Operation' && isset($updateIncident)) ? 'readonly' : '' }}>{{ isset($showIncident) ? $showIncident->brief_description : '' }}{{ isset($updateIncident) ? $updateIncident->brief_description : '' }}{{ !isset($updateIncident) && old('brief_description') ? old('brief_description') : '' }}</textarea>
							@if($errors->has('brief_description'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('brief_description') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('incident_details') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Incident Details</label>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<textarea name="incident_details" class="form-control" {{ isset($showIncident) || (Auth::user()->isUserType() == 'Operation' && isset($updateIncident)) ? 'readonly' : '' }}>{{ isset($showIncident) ? $showIncident->incident_details : '' }}{{ isset($updateIncident) ? $updateIncident->incident_details : '' }}{{ !isset($updateIncident) && old('incident_details') ? old('incident_details') : '' }}</textarea>
							@if($errors->has('incident_details'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('incident_details') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('other_comments') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Other Comments:</label>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<textarea name="other_comments" class="form-control" {{ isset($showIncident) || (Auth::user()->isUserType() == 'Operation' && isset($updateIncident)) ? 'readonly' : '' }}>{{ isset($showIncident) ? $showIncident->other_comments : '' }}{{ isset($updateIncident) ? $updateIncident->other_comments : '' }}{{ !isset($updateIncident) && old('other_comments') ? old('other_comments') : '' }}</textarea>
							@if($errors->has('other_comments'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('other_comments') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@can('access-matrix', 7)
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Attachments</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="form-group {{ $errors->has('attachments.*') ? 'has-error':'' }}">
				<div class="row">
					<div class="col-sm-12 attachmentsDiv">
						@if(!isset($showIncident) || (isset($updateIncident) && \Gate::allows('update-job-order', $updateIncident->id)))
						
						<input type="file" name="attachments[]" multiple {{ isset($showIncident)  ? 'readonly' : '' }} value="{{ !isset($updateIncident) && old('attachments.*') ? old('attachments.*') : '' }}">
						<p class="help-block">Accepted File Types: xlsx, xls, csv, jpg, jpeg, png, bmp, doc, docx, pdf, tif, tiff</p>

						@endif
						
						@if(isset($showIncident) || isset($updateIncident))
						
							@foreach($file as $f)
							<a href="{{ 'storage/'.$f->filename }}" target="_blank">{{ $f->filename }}</a> @isset($updateIncident) <a href="" class="deleteAttachment" data-id="{{ $f->id }}">  <i class="fa fa-close"></i></a> @endisset&emsp;
						
							@endforeach
						@endif
						
						@if($errors->has('attachments.*'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('attachments.*') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	@endcan

	@isset($showIncident)
	   	<div class="box box-primary">
	   		<div class="box-header with-border">
	   			<h3 class="box-title">Status Tracker</h3>
	   		</div>
	   		<!-- /.box-header -->

	   		<div class="box-body">
	   			<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	   				<thead>
	   					<tr>
	   						<th>Status</th>
	   						<th>Remarks</th>
	   						<th>Date & Time</th>
	   						<th>Updated By</th>
	   					</tr>
	   					@isset($status)
	   						@foreach ($status as $stat)
	   							<tr>
	   								<td>{{ $stat->status or '' }}</td>
	   								<td>{{ $stat->remarks or '' }}</td>
	   								<td>{{ $stat->created_at or '' }}</td>
	   								<td>{{ $stat->updated_by }}</td>
	   							</tr>
	   						@endforeach
	   					@endisset
	   				</thead>
	   			</table>
	   		</div>
	   	</div>
	@endisset

	@if(!isset($showIncident))
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="col-sm-6">
				<input type="submit" name="submit" hidden>
				<button type="button" id="submitBtn" data-loading-text="Loading..." class="btn btn-primary btn-block pull-right">Submit</button>
			</div>
			<div class="col-sm-6">
				<button type="button" id="cancelBtn" class="btn btn-default btn-block pull-right">Cancel</button>
			</div>
		</div>
	</div>
	@else
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			@can('access-matrix', 8)
			<div class="col-sm-6">
				<a href="/incidents/previewPdf/{{ $showIncident->id }}" target="_blank" id="previewPdf" data-loading-text="Loading..." class="btn btn-primary btn-block pull-right">Preview PDF</a>
			</div>
			<div class="col-sm-6">
				<a href="/incidents/downloadPdf/{{ $showIncident->id }}" id="downloadPdf" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Generating PDF" class="btn btn-primary btn-block pull-right">Download PDF</a>
			</div>
			@endcan
		</div>
	</div>
	@endif
</form>
@stop

@section('js')
<script>
	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': '{{ csrf_token() }}'
	        }
	});
	
	$(document).ready(function($){
		$('#remarksToolTip').tooltip(); 

		$('#remarksToolTip').on('show.bs.tooltip', function () {
			$('select[name="status"]').tooltip('show');
		})

		$('#remarksToolTip').on('hide.bs.tooltip', function () {
			$('select[name="status"]').tooltip('hide');
		})

		$('#project').select2({
			allowClear: true,
			ajax: {
				url: '/autocomplete/project',
				dataType: 'json',
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			}
		});

		$("#project").on('change', function(){
			$.ajax({
				url: '/projects/' + $(this).val(),
				type: 'GET',
				data: {type: 'populateProjects'},
				success: function(data, status){
					console.log(data);
					$('input[name="location"]').val(data.location);
					$('input[name="plant_id"]').val(data.plant_id);
					$('input[name="dc_capacity"]').val(data.dc_capacity);
				}
			});
		});

		@isset($updateIncident)
		    $('#status').select2({
		    	allowClear: true
		    });

		    //If user type is operation then set text area to readonly
		    @if(Auth::user()->isUserType() == 'Operation')
		    $('#project').select2({
		    	disabled: 'readonly',
		    	allowClear: true
		    });
		    @endif

		    //retrieve project name
		    var option = new Option('{{ $updateIncident->project_name }}', '{{ $updateIncident->project_id }}', true, true);
		    $('#project').append(option).trigger('change');
		    $('#project').trigger({
		    	type: 'select2:select',
		    	params: {
		    		data: '{{ $updateIncident->project_name }}'
		    	}
		    });

		    //retrieve status of incident
		    var option2 = new Option('{{ $updateIncident->status }}', '{{ $updateIncident->status }}', true, true);
		    $('[name="status"]').append(option2).trigger('change');
		    $('[name="status"]').trigger({
		    	type: 'select2:select',
		    	params: {
		    		data: '{{ $updateIncident->status }}'
		    	}
		    });

		    //activate status remarks on change of status value
		    var statusInitVal = $('select[name="status"]').val();
		    $('textarea[name="status_remarks"]').attr('readonly', 'readonly');
		    $('select[name="status"]').on('change', function(){
		    	if($(this).val() == statusInitVal){
		    		$('textarea[name="status_remarks"]').attr('readonly', 'readonly');
		    		$('textarea[name="status_remarks"]').val('');
		    	}else{
		    		$('textarea[name="status_remarks"]').removeAttr('readonly');
		    	}
		    })
		@endisset

		@if(isset($projectOld))
		var option = new Option('{{ $projectOld->project_name }}', '{{ $projectOld->id }}', true, true);
		$('#project').append(option).trigger('change');
		$('#project').trigger({
			type: 'select2:select',
			params: {
				data: '{{ $projectOld->project_name }}'
			}
		});
		@endif
	});

	$(document).on('click', '#submitBtn', function(e){
		swal({
			text: 'Are you sure you want to submit?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#submitBtn').button('loading');
			$('[type="submit"]').click();
		});
	})

	$(document).on('click', '#cancelBtn', function(){
		swal({
			text: 'Are you sure you want to cancel and discard prepared information?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/incidents";
		});
	})

	$(document).on('click', '.deleteAttachment', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		swal({
			text: 'Are you sure you want to delete this attachment?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$.ajax({
				url: '../' + id,
				type: 'DELETE',
				data: {type: 'deleteAttachment'},
				dataType: 'JSON',
				success: function(data, status){
					swal(data.title, data.msg, data.status).then(function(){
						window.location.reload();
					});
				}
			});
		});
	})

	var storedFiles= [];

	$(document).on('change', 'input[type="file"]', handleFileSelect);

	$(document).on('click', '.deleteUpload', function(e){
		e.preventDefault();
		console.log($('input[type="file"]').get(0).files[$(this).data('id')]);
		$('input[type="file"]').files[$(this).data('id')];
	});

	// $(document).on("click", ".selFile", removeFile);

	// $(document).on('submit', '#incidentSubmit', handleForm);

	function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function(f) {          

            storedFiles.push(f);
            
            var reader = new FileReader();
            reader.onload = function (e) {
                var html = "<div><i data-file='"+f.name+"' class='selFile fa fa-close' title='Click to remove'></i>" + f.name + "<br clear=\"left\"/></div>";
                $('.attachmentsDiv').append(html);
            }
            reader.readAsDataURL(f); 
        });
    }

    function removeFile(e) {
        var file = $(this).data("file");
        for(var i=0;i<storedFiles.length;i++) {
            if(storedFiles[i].name === file) {
                storedFiles.splice(i,1);
                break;
            }
        }
        $(this).parent().remove();
    }

    function handleForm(e) {
    	e.preventDefault();
    	var data = new FormData($('#incidentSubmit'));
        
        for(var i=0, len=storedFiles.length; i<len; i++) {
            data.append('files', storedFiles[i]); 
        }
        console.log(data);
    }

    $(document).on('click', '#downloadPdf', function(e){
    	e.preventDefault();
    	setTimeout(function(){
    		$('#downloadPdf').button('reset');
    		$('.top-right').notify({
    			message: { text: "Your download will start in a few seconds, please wait.." }
    		}).show();
    	}, 5000);
    	$(this).button('loading');
    	window.location = $(this).attr('href');
    });

</script>
@stop