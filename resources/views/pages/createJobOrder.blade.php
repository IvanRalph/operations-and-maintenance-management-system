@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>@if(isset($showJobOrder))View Job Order @elseif(isset($updateJobOrder))Update Job Order @else Create Job Order @endif</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="/job-orders"><i class="fa fa-file"></i> Job Orders</a></li>
	<li>@if(isset($showJobOrder))View Job Order @elseif(isset($updateJobOrder))Update Job Order @else Create Job Order @endif</li>
</ol>
@stop

@section('content')
<div class='notifications top-right'></div>
<!-- form start -->
<form action="{{ isset($updateJobOrder) ? action('JobOrderController@update', $updateJobOrder->id) : action('JobOrderController@store') }}" method="POST" enctype="multipart/form-data">
	@if(!isset($showJobOrder))
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	@endif
	@if(isset($updateJobOrder))
	<input type="hidden" name="_method" value="patch">
	@endif
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Incident Report Details</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('ir_no') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">IR No.</label>
						<div class="col-sm-6">
							@isset($showJobOrder)
							<input type="text" name="ir_no" class="form-control" value="{{ $showJobOrder->ir_no }}" readonly>
							@else
							<select id="ir_no" name="ir_no" class="form-control"></select>
							@endisset

							@if($errors->has('ir_no'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('ir_no') }}</span>
							@endif
						</div>

						@if(!isset($showJobOrder) || isset($updateJobOrder))
						<div class="col-sm-4">
							<input type="button" class="form-control btn btn-default" data-toggle="modal" data-target=".previewIrModal" name="preview_ir" disabled value="Preview IR">
						</div>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Project</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value="{{ isset($showJobOrder) ? $showJobOrder->project_name : '' }}" name="project" readonly>
						</div>
					</div>
				</div>

				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Plant ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value="{{ isset($showJobOrder) ? $showJobOrder->plant_id : '' }}" name="plant_id" readonly>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">Location</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value="{{ isset($showJobOrder) ? $showJobOrder->location : '' }}" name="location" readonly>
						</div>
					</div>
				</div>

				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">DC Capacity</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value="{{ isset($showJobOrder) ? $showJobOrder->dc_capacity : '' }}" name="dc_capacity" readonly>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Job Details</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="row">
				<div class="col-sm-6 form-group">
					<div class="row">
						<label class="col-sm-2">JO No.</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="" disabled value="{{ isset($showJobOrder) ? $showJobOrder->jo_no : '' }}{{ isset($updateJobOrder) ? $updateJobOrder->jo_no : '' }}{{ !isset($showJobOrder) && !isset($updateJobOrder) ? 'JO-000X' : '' }}">
						</div>
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('schedule') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Schedule</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" value="{{ isset($showJobOrder->schedule) ? date('Y-m-d', strtotime($showJobOrder->schedule)) : '' }}{{ isset($updateJobOrder) ? date('Y-m-d', strtotime($updateJobOrder->schedule)) : '' }}{{ !isset($updateJobOrder) && old('schedule') ? old('schedule') : '' }}" name="schedule" {{ isset($showJobOrder) || Auth::user()->isUserType() != 'Maintenance' ? 'disabled' : '' }}>
							@if($errors->has('schedule'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('schedule') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('asset_description') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Asset Description</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value="{{ isset($showJobOrder) ? $showJobOrder->asset_description : '' }}{{ isset($updateJobOrder) ? $updateJobOrder->asset_description : '' }}{{ !isset($updateJobOrder) && old('asset_description') ? old('asset_description') : '' }}" name="asset_description" {{ isset($showJobOrder) || (isset($updateJobOrder) && Auth::user()->isUserType() == 'Maintenance') ? 'readonly' : '' }}>
							@if($errors->has('asset_description'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('asset_description') }}</span>
							@endif
						</div>
					</div>
				</div>

				<div class="col-sm-6 form-group {{ $errors->has('priority_level') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-2">Priority Level</label>
						<div class="col-sm-10">
							@isset($showJobOrder)
							<input type="text" name="" class="form-control" value="{{ $showJobOrder->priority_level }}" readonly>
							@else
							<select name="priority_level" id="priority_level" class="form-control">
								<option value=""></option>
								<option value="Normal">Normal</option>
								<option value="High">High</option>
							</select>
							@endisset
							@if($errors->has('priority_level'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('priority_level') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('brief_description') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-3">Brief Description</label>
						<div class="col-sm-12">
							<textarea name="brief_description" class="form-control" {{ isset($showJobOrder) || (isset($updateJobOrder) && Auth::user()->isUsertype() == 'Maintenance') ? 'readonly' : '' }}>{{ isset($showJobOrder) ? $showJobOrder->brief_description : '' }}{{ isset($updateJobOrder) ? $updateJobOrder->brief_description : '' }}{{ !isset($updateJobOrder) && old('brief_description') ? old('brief_description') : '' }}</textarea>
							@if($errors->has('brief_description'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('brief_description') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('details') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-3">Details</label>
						<div class="col-sm-12">
							<textarea name="details" class="form-control" {{ isset($showJobOrder) || (isset($updateJobOrder) && Auth::user()->isUsertype() == 'Maintenance') ? 'readonly' : '' }}>{{ isset($showJobOrder) ? $showJobOrder->details : ''}}{{ isset($updateJobOrder) ? $updateJobOrder->details : '' }}{{ !isset($updateJobOrder) && old('details') ? old('details') : '' }}</textarea>
							@if($errors->has('details'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('details') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 form-group {{ $errors->has('remarks') ? 'has-error':'' }}">
					<div class="row">
						<label class="col-sm-3">Remarks</label>
						<div class="col-sm-12">
							<textarea name="remarks" class="form-control" {{ isset($showJobOrder) || (isset($updateJobOrder) && Auth::user()->isUsertype() == 'Maintenance') ? 'readonly' : '' }}>{{ isset($showJobOrder) ? $showJobOrder->remarks : '' }}{{ isset($updateJobOrder) ? $updateJobOrder->remarks : '' }}{{ !isset($updateJobOrder) && old('remarks') ? old('remarks') : '' }}</textarea>
							@if($errors->has('remarks'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('remarks') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@can('access-matrix', 14)
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Attachments</h3>
		</div>
		<!-- /.box-header -->

		<div class="box-body">
			<div class="form-group {{ $errors->has('attachments.*') ? 'has-error':'' }}">
				<div class="row">
					<div class="col-sm-12">
						@if((!isset($showJobOrder) && !isset($updateJobOrder)) || (isset($updateJobOrder) && \Gate::allows('update-job-order', $updateJobOrder->id) && Auth::user()->isUserType() != 'Maintenance'))
						<input type="file" name="attachments[]" multiple>
						<p class="help-block">Accepted File Types: xlsx, xls, csv, jpg, jpeg, png, bmp, doc, docx, pdf, tif, tiff</p>
						@endif
						@if(isset($showJobOrder) || isset($updateJobOrder))
						@foreach($file as $f)
						<a href="{{ '../storage/'.$f->filename }}" target="_blank">{{ $f->filename }}</a> @if(isset($updateJobOrder) && \Gate::allows('update-job-order', $updateJobOrder->id) && Auth::user()->isUserType() != 'Maintenance') <a href="" class="deleteAttachment" data-id="{{ $f->id }}">  <i class="fa fa-close"></i></a> @endif&emsp;
						@endforeach
						@endif
						@if($errors->has('attachments'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('attachments') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	@endcan

	@if(!isset($showJobOrder))
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="col-sm-6">
				<input type="submit" name="submit" hidden>
				<button type="button" id="submitBtn" data-loading-text="Loading..." class="btn btn-primary btn-block pull-right">Submit</button>
			</div>
			<div class="col-sm-6">
				<button type="button" id="cancelBtn" class="btn btn-default btn-block pull-right">Cancel</button>
			</div>
		</div>
	</div>
	@else
		@can('access-matrix', 15)
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="col-sm-6">
					<a href="/job-orders/previewPdf/{{ $showJobOrder->id }}" target="_blank" id="previewPdf" data-loading-text="Loading..." class="btn btn-primary btn-block pull-right">Preview PDF</a>
				</div>
				<div class="col-sm-6">
					<a href="/job-orders/downloadPdf/{{ $showJobOrder->id }}" id="downloadPdf" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Generating PDF" class="btn btn-primary btn-block pull-right">Download PDF</a>
				</div>
			</div>
		</div>
		@endcan
	@endif
</form>

{{-- PREVIEW-MODAL --}}
<div class="modal fade previewIrModal" tabindex="-1" role="dialog" aria-labelledby="previewIr">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modalTitle"></h4>
			</div>
			<div class="modal-body">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">General Details</h3>
					</div>
					<!-- /.box-header -->

					<div class="box-body">
						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">Project</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="project" value="" readonly>
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-group">
								<div class="row">
									<label class="col-sm-2">IR No.</label>
									<div class="col-sm-6"><input type="text" name="ir_no" class="form-control" readonly value=""></div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="row">
									<label class="col-sm-1">Location</label>
									<div class="col-sm-11">
										<input type="text" name="location" class="form-control" readonly value="">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group row">
									<label class="col-sm-2">Plant ID</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="plant_id" readonly value="">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2">DC Capacity</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="dc_capacity" readonly value="">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2">Status</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="status" value="" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Incident Details</h3>
					</div>
					<!-- /.box-header -->

					<div class="box-body">
						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="row">
									<label class="col-sm-2">Brief Description</label>
								</div>

								<div class="row">
									<div class="col-sm-12">
										<textarea name="brief_description" class="form-control" readonly></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="row">
									<label class="col-sm-2">Incident Details</label>
								</div>

								<div class="row">
									<div class="col-sm-12">
										<textarea name="incident_details" class="form-control" readonly></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 form-group">
								<div class="row">
									<label class="col-sm-2">Other Comments:</label>
								</div>

								<div class="row">
									<div class="col-sm-12">
										<textarea name="other_comments" class="form-control" readonly></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Attachments</h3>
					</div>
					<!-- /.box-header -->

					<div class="box-body">
						<div class="form-group">
							<div class="row">
								<div class="col-sm-12">
									<div class="attachments"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Status Tracker</h3>
					</div>
					<!-- /.box-header -->

					<div class="box-body">
						<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
							<thead>
								<tr>
									<th>Status</th>
									<th>Remarks</th>
									<th>Date & Time</th>
									<th>Updated By</th>
								</tr>
							</thead>
							<tbody id="modalTracker">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
@stop

@section('js')
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': '{{ csrf_token() }}'
		}
	});

	$(document).ready(function(){
		$('#ir_no').select2({
			allowClear: true,
			ajax: {
				url: '/autocomplete/incident',
				dataType: 'json',
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			}
		});
	});

	$('#priority_level').select2({
		allowClear: true
	});

	$("#ir_no").on('change', function(){
		$.ajax({
			url: '/incidents/' + $(this).val(),
			type: 'GET',
			data: {type: 'populateIncidents'},
			success: function(data, status){
				$('input[name="location"]').val(data.location);
				$('input[name="dc_capacity"]').val(data.dc_capacity);
				$('input[name="plant_id"]').val(data.plant_id);
				$('input[name="project"]').val(data.project_name);

				$('#modalTitle').html(data.ir_no + ' - ' + data.project_name);
				$('input[name="ir_no"]').val(data.ir_no);
				$('input[name="status"]').val(data.status);
				$('textarea[name="brief_description"]:last').val(data.brief_description);
				$('textarea[name="incident_details"]').val(data.incident_details);
				$('textarea[name="other_comments"]').val(data.other_comments);

				$.each(data.file, function(key,value){
					$('.attachments').append('<a href="../incidents/storage/'+ value.filename +'">' + value.filename + '</a>&emsp;')
				});

				$.each(data.tracker, function(key, value){
					$('#modalTracker').append('<tr><td>'+ value.status +'</td><td>' + value.remarks + '</td><td>' + value.created_at + '</td><td>' + value.updated_by + '</td></tr>')
				});

				$('input[name="preview_ir"]').removeAttr('disabled');
			}
		});
	});

	@if(old('ir_no'))
	var option = new Option('{{ old('ir_no') }}', '{{ old('ir_no') }}', true, true);
	$('#ir_no').append(option).trigger('change');
	$('#ir_no').trigger({
		type: 'select2:select',
		params: {
			data: '{{ old('ir_no') }}'
		}
	});
	@endif

	@if(isset($updateJobOrder))
		@if(Auth::user()->isUserType() == 'Maintenance')
		$('#ir_no').select2({
			disabled: 'readonly',
			allowClear: true
		});

		$('#priority_level').select2({
			disabled: 'readonly',
			allowClear: true
		});
		@endif

	//retrieve ir no
	var option = new Option('{{ $updateJobOrder->ir_no }}', '{{ $updateJobOrder->ir_no }}', true, true);
	$('#ir_no').append(option).trigger('change');
	$('#ir_no').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateJobOrder->ir_no }}'
		}
	});

	//retrieve priority level
	var option = new Option('{{ $updateJobOrder->priority_level }}', '{{ $updateJobOrder->priority_level }}', true, true);
	$('#priority_level').append(option).trigger('change');
	$('#priority_level').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateJobOrder->priority_level }}'
		}
	});
	@endif

	$(document).on('click', '#submitBtn', function(e){
		swal({
			text: 'Are you sure you want to submit?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#submitBtn').button('loading');
			$('[type="submit"]').click();
		});
	})

	$(document).on('click', '#cancelBtn', function(){
		swal({
			text: 'Are you sure you want to cancel and discard prepared information?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/job-orders";
		});
	})

	$(document).on('click', '.deleteAttachment', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		swal({
			text: 'Are you sure you want to delete this attachment?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$.ajax({
				url: '../' + id,
				type: 'DELETE',
				data: {type: 'deleteAttachment'},
				dataType: 'JSON',
				success: function(data, status){
					swal(data.title, data.msg, data.status).then(function(){
						window.location.reload();
					});
				}
			});
		});
	})

	$(document).on('click', '#downloadPdf', function(e){
		e.preventDefault();
		setTimeout(function(){
			$('#downloadPdf').button('reset');
			$('.top-right').notify({
				message: { text: "Your download will start in a few seconds, please wait.." }
			}).show();
		}, 5000);
		$(this).button('loading');
		window.location = $(this).attr('href');
	});
</script>
@stop