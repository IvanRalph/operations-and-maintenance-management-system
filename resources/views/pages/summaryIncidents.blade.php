@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Summary Reports > Incidents</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Summary Reports</li>
	<li>Incidents</li>
</ol>
@stop

@section('content')
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>IR No.</th>
			<th>IR Created Date</th>
			<th>IR Updated Date</th>
			<th>Project</th>
			<th>Location</th>
			<th>Plant ID</th>
			<th>DC Capacity</th>
			<th>Brief Description</th>
			<th>Incident Details</th>
			<th>Other Comments</th>
			<th>Prepared By</th>
			<th>Status</th>
			<th>Status Date</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/incidentData',
		columns: [
		{ data: 'ir_no', visible: true, width: '60px' },
		{ data: 'created_at', visible: true, width: '100px' },
		{ data: 'updated_at', visible: true, width: '100px' },
		{ data: 'project_name', visible: true, width: '100%' },
		{ data: 'location', visible: true },
		{ data: 'plant_id', visible: true },
		{ data: 'dc_capacity', visible: true },
		{ data: 'brief_description', visible: true },
		{ data: 'incident_details', visible: true },
		{ data: 'other_comments', visible: true },
		{ data: 'prepared_by', visible: true, width: '150px' },
		{ data: 'status', visible: true },
		{ data: 'status_date', visible: true, width:'100px' }
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5,6,7,8,9,10,11,12]
			}
		},
		],
		columnDefs: [
			{
				targets: [3,4,7,8,9],
				render: function(data, type, row){
					if(data != null){
						return data.length > 10 ?
						'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
						data;
					}else{
						return data;
					}
				}
			}
		],
		pageLength: 10,
		"scrollX": true,
		"fixedHeader": true
	});

		$('#mainTable').on('draw.dt', function () {
	        $('[data-toggle="tooltip"]').tooltip();
	    });
</script>
@stop