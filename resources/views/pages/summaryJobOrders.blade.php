@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Summary Reports > Job Orders</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Summary Reports</li>
	<li>Job Orders</li>
</ol>
@stop

@section('content')
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>JO No.</th>
			<th>JO Created Date</th>
			<th>JO Updated Date</th>
			<th>IR No.</th>
			<th>Project</th>
			<th>Location</th>
			<th>Plant ID</th>
			<th>DC Capacity</th>
			<th>Asset Description</th>
			<th>Brief Description</th>
			<th>Priority Level</th>
			<th>Details</th>
			<th>Remarks</th>
			<th>Schedule</th>
			<th>Issued By</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/jobOrderData',
		columns: [
		{ data: 'jo_no', width: '75px' },
		{ data: 'created_at', width: '100px' },
		{ data: 'updated_at', width: '100px' },
		{ data: 'ir_no', width: '75px' },
		{ data: 'project_name' },
		{ data: 'location' },
		{ data: 'plant_id' },
		{ data: 'dc_capacity' },
		{ data: 'asset_description' },
		{ data: 'brief_description' },
		{ data: 'priority_level' },
		{ data: 'details' },
		{ data: 'remarks' },
		{ data: 'schedule', width: '100px' },
		{ data: 'issued_by', width: '150px' },
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]
			}
		}
		],
		columnDefs: [
			{
				targets: [4,5,8,9,10,11,12],
				render: function(data, type, row){
					if(data != null){
						return data.length > 10 ?
						'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
						data;
					}else{
						return data;
					}
				}
			}
		],
		pageLength: 10,
		"scrollX": true,
		"fixedHeader": true
	});

	$('#mainTable').on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop