@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Summary Reports > Field Activities</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Summary Reports</li>
	<li>Field Activities</li>
</ol>
@stop

@section('content')
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>FAR No.</th>
			<th>FAR Created Date</th>
			<th>FAR Updated Date</th>
			<th>IR No.</th>
			<th>JO No.</th>
			<th>Project</th>
			<th>Location</th>
			<th>DC Capacity</th>
			<th>Asset Description</th>
			<th>Issue Description</th>
			<th>Observation</th>
			<th>Actions</th>
			<th>Remarks</th>
			<th>In-Charge</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/fieldActivityData',
		columns: [
		{ data: 'far_no', width: '75px' },
		{ data: 'created_at', width: '100px' },
		{ data: 'updated_at', width: '100px' },
		{ data: 'ir_no', width: '75px' },
		{ data: 'jo_no', width: '75px' },
		{ data: 'project_name' },
		{ data: 'location' },
		{ data: 'dc_capacity' },
		{ data: 'asset_description' },
		{ data: 'issue_description' },
		{ data: 'observation' },
		{ data: 'actions' },
		{ data: 'remarks' },
		{ data: 'in_charge', width: '150px' },
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
			}
		}
		],
		columnDefs: [
			{
				targets: [5,6,8,9,10,11,12],
				render: function(data, type, row){
					if(data != null){
						return data.length > 10 ?
						'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
						data;
					}else{
						return data;
					}
				}
			}
		],
		pageLength: 10,
		"scrollX": true,
		"fixedHeader": true
	});

	$('#mainTable').on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop