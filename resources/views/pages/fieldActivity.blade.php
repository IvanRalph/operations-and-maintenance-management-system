@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Field Activity</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Field Activity</li>
</ol>
@stop

@section('content')
<div class='notifications top-right'></div>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>FAR No.</th>
			<th>FAR Created Date</th>
			<th>FAR Updated Date</th>
			<th>JO No.</th>
			<th>Project</th>
			<th>DC Capacity</th>
			<th>Issue Description</th>
			<th>Observation</th>
			<th>Actions</th>
			<th>In-Charge</th>
			<th>Action Items</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': '{{ csrf_token() }}'
	        }
	});
	@if(session()->has('fieldActivityCreated'))
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Field Activity Report has been created." }
		}).show();
	});
	@endif

	@if(session()->has('fieldActivityUpdated'))
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Field Activity Report has been updated." }
		}).show();
	});
	@endif
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/fieldActivityData',
		columns: [
		{ data: 'far_no', visible: true, width: '60px' },
		{ data: 'created_at', visible: true, width: '100px' },
		{ data: 'updated_at', visible: true, width: '110px' },
		{ data: 'jo_no', visible: true, width: '60px' },
		{ data: 'project_name', visible: true, width: '100%' },
		{ data: 'dc_capacity', visible: true, width: '60px' },
		{ data: 'issue_description', visible: true, width: '60px' },
		{ data: 'observation', visible: true, width: '60px' },
		{ data: 'actions', visible: true, width: '60px' },
		{ data: 'in_charge', visible: true, width: '150px' },
		{ data: null, width: '200px', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"l {{ \Gate::allows('access-matrix', 16) ? 'f' : '' }} >tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn = "<a href='/field-activities/"+ data.id +"' class='btn btn-default'>View</a>";
				@can('access-matrix', 19)
				btn += "<a href='/field-activities/"+data.id+"/edit' class='btn btn-default'>Update</a>";
				@endcan
				@can('access-matrix', 20)
				if(data.in_charge == '{{ \Auth::user()->getDetails()->firstname . ' ' . \Auth::user()->getDetails()->lastname }}'){
					btn += "<a href='' data-id='" + data.id + "' class='btn btn-default deleteBtn'>Delete</a>";
				}
				@endcan

				return btn;
			}
		},
		{
			targets: [4,6,7,8],
			render: function(data, type, row){
				if(data != null){
					return data.length > 10 ?
					'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
					data;
				}else{
					return data;
				}
			}
		}
		],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5,6,7,8,9]
			}
		},
		@can('access-matrix', 17)
		{
			text: 'Create Field Activity Report',
			action: function ( e, dt, node, config ) {
				window.location.href="/field-activities/create";
			}
		}
		@endcan
		],
		"scrollX": true,
		"fixedHeader": true
	});

	$('#mainTable').on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

	$(document).on('click', '.deleteBtn', function(e){
		e.preventDefault();
		swal({
			text: 'Are you sure you want to delete this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$.ajax({
				url: 'field-activities/' + $('.deleteBtn').data('id'),
				type: 'DELETE',
				dataType: 'JSON',
				success: function(data, result){
					swal(data.title, data.msg, data.type).then(function(){
						table.ajax.reload();
					});
				}
			});
		});
	})
</script>
@stop