@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Setup > User Access</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Setup</li>
	<li>User Access</li>
</ol>
@stop

@section('content')
<div class='notifications top-right'></div>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>Fullname</th>
			<th>Department</th>
			<th>User Types</th>
			<th>Updated Date</th>
			<th>Updated By</th>
			<th>Action Items</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	@isset($userUpdated)
	$(document).ready(function(){
		console.log('test');
		$('.top-right').notify({
			message: { text: "{{ $userUpdated->firstname. ' ' . $userUpdated->lastname }}'s user account has been updated." }
		}).show();
	});
	@endisset
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/userAccessData',
		columns: [
		{ data: 'full_name', widht: '100%' },
		{ data: 'department' },
		{ data: 'user_type' },
		{ data: 'updated_at' },
		{ data: 'updated_by' },
		{ data: null, width: '200px', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn = "<a href='/users/"+ data.id +"' class='btn btn-default'>View</a>";
				btn += "<a href='/users/"+data.id+"/edit' class='btn btn-default'>Update</a>";

				return btn;
			}
		},
		{
			targets: [2],
			render: function(data, type, row){
				if(data == null){
					return "No user type";
				}else{
					return data;
				}
			}
		}
		],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4]
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});
</script>
@stop