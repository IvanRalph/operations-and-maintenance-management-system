@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>@if(isset($showProject)) View {{ $showProject->project_name }} Project @elseif(isset($updateProject)) Update {{ $updateProject->project_name }} @else Create Project @endif</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="/projects"> Projects</a></li>
	<li>Create Project</li>
</ol>
@stop

@section('content')

<div class="box box-primary">
	<div class="box-body">
		<!-- form start -->
		<form class="form-horizontal col-sm-6 col-sm-offset-3" action="{{ isset($updateProject) ? action('ProjectController@update', $updateProject->id) : action('ProjectController@store') }}" method="POST">
			@if(!isset($showProject))
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@endif
			@if(isset($updateProject))
			<input type="hidden" name="_method" value="patch">
			@endif
			<div class="form-group {{ $errors->has('project_name') ? 'has-error':'' }}">
				<label class="col-sm-3">Project Name</label>

				<div class="col-sm-9">
					<input class="form-control" type="text" name="project_name" value="{{ !isset($updateProject) && old('project_name') ? old('project_name') : '' }}{{ isset($showProject) ? $showProject->project_name : '' }}{{ isset($updateProject) ? $updateProject->project_name : '' }}"  {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('project_name'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('project_name') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3">Plant ID</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="plant_id" disabled value="{{ isset($showProject) ? $showProject->plant_id : '' }}{{ isset($updateProject) ? $updateProject->plant_id : '' }}{{ !isset($showProject) && !isset($updateProject) ? 'PID-000X' : '' }}">
				</div>
			</div>

			<div class="form-group {{ $errors->has('plant_type') ? 'has-error':'' }}">
				<label class="col-sm-3">Plant Type</label>

				<div class="col-sm-9">
					<select class="form-control" name="plant_type" {{ isset($showProject) ? 'readonly' : '' }}>
						<option></option>
						<option value="PPA" {{ isset($showProject) && $showProject->plant_type == 'PPA' ? 'selected' : '' }}{{ isset($updateProject) && $updateProject->plant_type == 'PPA' ? 'selected' : '' }}>PPA</option>
						<option value="EPC" {{ isset($showProject) && $showProject->plant_type == 'EPC' ? 'selected' : '' }}{{ isset($updateProject) && $updateProject->plant_type == 'EPC' ? 'selected' : '' }}>EPC</option>
						<option value="FIT" {{ isset($showProject) && $showProject->plant_type == 'FIT' ? 'selected' : '' }}{{ isset($updateProject) && $updateProject->plant_type == 'FIT' ? 'selected' : '' }}>FIT</option>
					</select>
					@if($errors->has('plant_type'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('plant_type') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group {{ $errors->has('dc_capacity') ? 'has-error':'' }}">
				<label class="col-sm-3">DC Capacity</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="dc_capacity" value="{{ !isset($updateProject) && old('dc_capacity') ? old('dc_capacity') : '' }}{{ isset($showProject) ? $showProject->dc_capacity : '' }}{{ isset($updateProject) ? $updateProject->dc_capacity : '' }}" {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('dc_capacity'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('dc_capacity') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group {{ $errors->has('ac_capacity') ? 'has-error':'' }}">
				<label class="col-sm-3">AC Capacity</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="ac_capacity" value="{{ !isset($updateProject) && old('ac_capacity') ? old('ac_capacity') : '' }}{{ isset($showProject) ? $showProject->ac_capacity : '' }}{{ isset($updateProject) ? $updateProject->ac_capacity : '' }}" {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('ac_capacity'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('ac_capacity') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group {{ $errors->has('location') ? 'has-error':'' }}">
				<label class="col-sm-3">Location</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="location" value="{{ !isset($updateProject) && old('location') ? old('location') : '' }}{{ isset($showProject) ? $showProject->location : '' }}{{ isset($updateProject) ? $updateProject->location : '' }}" {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('location'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('location') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group {{ $errors->has('date_commissioned') ? 'has-error':'' }}">
				<label class="col-sm-3">Date Commissioned</label>

				<div class="col-sm-9">
					<input class="form-control"  type="date" name="date_commissioned" value="{{ !isset($updateProject) && old('date_commissioned') ? old('date_commissioned') : '' }}{{ isset($showProject) ? $showProject->date_commissioned : '' }}{{ isset($updateProject) ? $updateProject->date_commissioned : '' }}" {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('date_commissioned'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('date_commissioned') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group {{ $errors->has('pv_module') ? 'has-error':'' }}">
				<label class="col-sm-3">PV Module</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="pv_module" value="{{ !isset($updateProject) && old('pv_module') ? old('pv_module') : '' }}{{ isset($showProject) ? $showProject->pv_modules : '' }}{{ isset($updateProject) ? $updateProject->pv_modules : '' }}" {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('pv_module'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('pv_module') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group {{ $errors->has('inverter') ? 'has-error':'' }}">
				<label class="col-sm-3">Inverter</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="inverter" value="{{ !isset($updateProject) && old('inverter') ? old('inverter') : '' }}{{ isset($showProject) ? $showProject->inverter : '' }}{{ isset($updateProject) ? $updateProject->inverter : '' }}" {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('inverter'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('inverter') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group {{ $errors->has('communication') ? 'has-error':'' }}">
				<label class="col-sm-3">Communication</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="communication" value="{{ !isset($updateProject) && old('communication') ? old('communication') : '' }}{{ isset($showProject) ? $showProject->communication : '' }}{{ isset($updateProject) ? $updateProject->communication : '' }}" {{ isset($showProject) ? 'readonly' : '' }}>
					@if($errors->has('communication'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('communication') }}</span>
					@endif
				</div>
			</div>

			@if(!isset($showProject))
			<div class="row">
				<div class="col-sm-6">
					<input type="submit" class="btn btn-primary btn-block pull-right" name="submit" value="Save">
				</div>

				<div class="col-sm-6">
					<input type="button" class="btn btn-default btn-block pull-right" name="cancel" value="Cancel">
				</div>
			</div>
			@endif
		</form>
	</div>
</div>
@stop

@section('js')
<script>
	$('input[name="submit"]').on('click', function(){
		$(this).button('loading');
	});
</script>
@stop