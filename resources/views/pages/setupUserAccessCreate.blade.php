@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>User Access</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Setup</li>
	<li>User Access</li>
</ol>
@stop

@section('content')

<div class="box box-primary">
	<div class="box-header with-border">
	</div>
	<div class="box-body">
		<!-- form start -->
		<form class="form-horizontal col-sm-6 col-sm-offset-3" action="{{ isset($employee) ? action('UserAccessController@update', $employee->employee_id) : '' }}" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="_method" value="patch">
			<div class="form-group">
				<label class="col-sm-3">Full Name</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="full_name" disabled value="{{ isset($employee) ? $employee->full_name : '' }}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3">Department</label>

				<div class="col-sm-9">
					<input class="form-control"  type="text" name="Department" disabled value="{{ isset($employee) ? $employee->department : '' }}">
				</div>
			</div>

			<div class="form-group {{ $errors->has('user_type') ? 'has-error':'' }}">
				<label class="col-sm-3">User Type</label>

				<div class="col-sm-9">
					<select name="user_type" class="form-control" {{ isset($employee) && !isset($employee->type) ? 'disabled' : '' }}>
						<option {{ isset($employee) && $employee->user_type_id != 1 && $employee->user_type_id != 2 ? 'selected' : '' }}></option>
						<option value="1" {{ isset($employee) && $employee->user_type_id == 1 ? 'selected' : '' }}>Operations</option>
						<option value="2" {{ isset($employee) && $employee->user_type_id == 2 ? 'selected' : '' }}>Maintenance</option>
					</select>
					@if($errors->has('user_type'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('user_type') }}</span>
					@endif
				</div>
			</div>

			@if(isset($employee->type))
				<div class="row">
					<div class="col-sm-6">
						<button type="submit" class="btn btn-primary btn-block pull-right">Save</button>
					</div>

					<div class="col-sm-6">
						<button type="button" class="btn btn-default btn-block pull-right">Cancel</button>
					</div>
				</div>
			@endif
		</form>
	</div>

	<div class="box-footer">
		
	</div>
</div>
@stop