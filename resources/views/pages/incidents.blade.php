@extends('adminlte::page')

@section('title', 'Operations & Maintenance Management System')

@section('content_header')
<h1>Incidents</h1>
<ol class="breadcrumb">
	<li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li>Incidents</li>
</ol>
@stop

@section('content')
<div class='notifications top-right'></div>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>IR No.</th>
			<th>IR Created Date</th>
			<th>IR Updated Date</th>
			<th>Project</th>
			<th>Location</th>
			<th>Plant ID</th>
			<th>DC Capacity</th>
			<th>Prepared By</th>
			<th>Status</th>
			<th>Status Date</th>
			<th>Action Items</th>
		</tr>
	</thead>	
</table>

@stop

@section('js')
<script>
	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': '{{ csrf_token() }}'
	        }
	});
	@if(session()->has('incidentCreated'))
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Incident Report has been created." }
		}).show();
	});
	@endif
	@if(session()->has('incidentUpdated'))
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Incident Report has been updated." }
		}).show();
	});
	@endif
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/incidentData',
		columns: [
		{ data: 'ir_no', visible: true, width: '60px' },
		{ data: 'created_at', visible: true, width: '100px' },
		{ data: 'updated_at', visible: true, width: '100px' },
		{ data: 'project_name', visible: true, width: '100%' },
		{ data: 'location', visible: true },
		{ data: 'plant_id', visible: false },
		{ data: 'dc_capacity', visible: true },
		{ data: 'prepared_by', visible: true, width: '100%' },
		{ data: 'status', visible: true },
		{ data: 'status_date', visible: false },
		{ data: null, width: '200px', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"l @can('access-matrix', 2) f @endcan >tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn ="";
				@can('access-matrix', 4)
				btn = "<a href='/incidents/"+ data.id +"' class='btn btn-default'>View</a>";
				@endcan
				@can('access-matrix', 5)
					@if(Auth::user()->isUserType() == 'Maintenance')
						if((data.status == 'Open' || data.status == 'Reopen') && data.prepared_by == '{{ Auth::user()->getDetails()->firstname . ' ' . Auth::user()->getDetails()->lastname }}'){
							btn += "<a href='/incidents/"+data.id+"/edit' class='btn btn-default'>Update</a>";
						}
					@elseif(Auth::user()->isUserType() == 'Operation')
						btn += "<a href='/incidents/"+data.id+"/edit' class='btn btn-default'>Update</a>";
					@endif
				@endcan
				
				@can('access-matrix', 6)
				if(data.status == 'Open' && data.prepared_by == '{{ Auth::user()->getDetails()->firstname . ' ' . Auth::user()->getDetails()->lastname }}'){
					btn += "<a href='#' data-id='"+ data.id +"' class='btn btn-default deleteBtn'>Delete</a>";
				}
				@endcan
				
				return btn;
			}
		},
		{
			targets: [3,4],
			render: function(data, type, row){
				if(data != null){
					return data.length > 10 ?
					'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
					data;
				}else{
					return data;
				}
			}
		}
		],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [0,1,2,3,4,5,6,7,8,9]
			}
		},
		@can('access-matrix', 3)
		{
			text: 'Create Incident Report',
			action: function ( e, dt, node, config ) {
				window.location.href="/incidents/create";
			}
		}
		@endcan
		],
		"scrollX": true,
		"fixedHeader": true
	});

	$('#mainTable').on('draw.dt', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

	@can('access-matrix', 6)
	$(document).on('click', '.deleteBtn', function(e){
		e.preventDefault();
		swal({
			text: 'Are you sure you want to delete this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$.ajax({
				url: '../incidents/' + $('.deleteBtn').data('id'),
				type: 'DELETE',
				dataType: 'JSON',
				success: function(data, result){
					swal(data.title, data.msg, data.type).then(function(){
						table.ajax.reload();
					});
				}
			});
		});
	})
	@endcan
</script>
@stop