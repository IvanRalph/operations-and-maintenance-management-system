@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}"><img src="/img/solar-logo-sm.png" width="80%"></a>
        </div>

        <h3 class="text-center" style="color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 200;">Operations & Maintenance Management System</h3>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ trans('adminlte::adminlte.login_message') }}</p>
            <form action="{{ isset($emailValidated) || isset($invalidPass) ? route('login') : route('validate_login') }}" method="post" id="loginForm">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('email') || isset($invalidPass) || isset($emailNotExist) || isset($userInactive) || isset($noAccess) ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                @if(isset($emailValidated) || isset($invalidPass) || isset($emailNotExist) || isset($userInactive))
                <div class="form-group has-feedback {{ $errors->has('password') || isset($invalidPass) || isset($emailNotExist) || isset($userInactive) ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    @isset($invalidPass)
                        <span class="help-block">
                            <strong>Incorrect login details</strong>
                        </span>
                    @endisset

                    @isset($emailNotExist)
                        <span class="help-block">
                            <strong>Email does not exist</strong>
                        </span>
                    @endisset

                    @isset($userInactive)
                        <span class="help-block">
                            <strong>Your account is inactive, contact an <a href="mailto:it.enterprise@solarphilippines.ph">IT Enterprise Administrator</a> to reactivate your account.</strong>
                        </span>
                    @endisset
                </div>
                @endif
                @isset($noUserType)
                    <span class="help-block">
                        <strong>No user type associated with your account, contact your supervisor to gain access.</strong>
                    </span>
                @endisset
                
                @isset($noAccess)
                    <span class="help-block">
                        <strong>You have no access to this system, if you think otherwise please contact <a href="mailto:it.enterprise@solarphilippines.ph">IT Enterprise Applications Division</a> to gain access.</strong>
                    </span>
                @endisset
                @isset($noPass)
                    <span class="help-block">
                        <strong class="text-center">We have detected that it's your first time signing in on the system, a system generated password is sent to your e-mail</strong>
                    </span>
                @endisset
                <div class="row">
                    <div class="col-xs-8">
                        <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"
                           class="text-center"
                        >Forgot Password</a>
                    </div>
                    <div class="col-xs-4 pull-right">
                        <input type="submit" name="submit" data-loading-text="Loading..." class="btn btn-primary btn-block btn-flat" value="{{ isset($emailValidated) || isset($invalidPass) ? 'Sign In' : 'Validate' }}">
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <div class="auth-links">
                
                <br>
            </div>
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });

        $("#loginForm").on('submit', function(e){
            $("input[name='submit']").button('loading');
        })
    </script>
    @yield('js')
@stop
